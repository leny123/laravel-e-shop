<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Znacka;
use Illuminate\Support\Facades\Schema;
class ZnackyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Znacka::truncate();
        Schema::enableForeignKeyConstraints();
        
        Znacka::create(['display_name' => 'Champion']);
        Znacka::create(['display_name' => 'Tommy']);
        Znacka::create(['display_name' => 'Calvin_Klein']);
        Znacka::create(['display_name' => 'Adidas']);
        Znacka::create(['display_name' => 'Nike']);
    }
}
