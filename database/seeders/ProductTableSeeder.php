<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Kategorie;
use App\Models\Podkategorie;
use Illuminate\Support\Facades\Schema;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() { 
        Schema::disableForeignKeyConstraints();
        Product::truncate();
        Schema::enableForeignKeyConstraints();
        $jmena = ['Tričko', 'Mikina', 'Ponožky', 'Boty'];
        $gender = ['f', 'm'];
        $kategorie = Kategorie::all();
        $podkategorie = Podkategorie::all();
        foreach($kategorie as $category) {
            $category->products()->detach();
        }
        foreach($podkategorie as $podcategory) {
            $podcategory->products()->detach();
        }
        
        $znacky = ['Champion', 'Tommy Hilfiger', 'Calvin Klein', 'Adidas', 'Nike'];
        $boolean = [true, false];
        for($i = 0; $i < 500; $i++) {
            ($boolean[rand(0,1)]) ? $sleva = rand(199, 399) : $sleva = 0;
            $data[] = [
                'name' => $jmena[rand(0,3)],
                'gender' => $gender[rand(0,1)],
                'znacka' => $znacky[rand(0,4)],
                'sleva' => $sleva,
                'fotka' => 'none',
                'cena' => rand(499, 1000),
                'popis' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque harum voluptatum ratione delectus similique repellat est, placeat iure veniam illo, deserunt quod nemo. Labore vitae maiores perferendis facere, sequi qui.',
                'bought' => 0
            ];
        }
        $chunks = array_chunk($data, 100);
        foreach($chunks as $chunk) {
            Product::insert($chunk);
        }
        // Pro rychlejší seeding, stačí seedovat bez pivot tablu.
        $products = Product::all();
        foreach($products as $product) {
            $product->kategorie()->sync($kategorie[rand(0,2)]);
            $product->podkategorie()->sync($podkategorie[rand(0,2)]);
        }
    }
}
