<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kategorie;
use Illuminate\Support\Facades\Schema;

class KategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Kategorie::truncate();
        Schema::enableForeignKeyConstraints();
        Kategorie::create(['display_name' => 'Oblečení', 'name' => 'obleceni']);
        Kategorie::create(['display_name' => 'Boty', 'name' => 'boty']);
        Kategorie::create(['display_name' => 'Spodní prádlo', 'name' => 'spodni_pradlo']);
    }
}
