<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Size;
use Illuminate\Support\Facades\Schema;
class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Size::truncate();
        Schema::enableForeignKeyConstraints();
        
        Size::create(['sizes' => 'XS']);
        Size::create(['sizes' => 'S']);
        Size::create(['sizes' => 'M']);
        Size::create(['sizes' => 'L']);
        Size::create(['sizes' => 'XL']);

    }
}
