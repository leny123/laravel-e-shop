<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Podkategorie;
use Illuminate\Support\Facades\Schema;

class PodKategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Podkategorie::truncate();
        Schema::enableForeignKeyConstraints();
        Podkategorie::create([
            'kategorie_id' => 1,
            'display_name' => 'Trička',
            'name' => 'tricka'
            ]);
        Podkategorie::create([
            'kategorie_id' => 2,
            'display_name' => 'Tenisky',
            'name' => 'tenisky'
            ]);
        Podkategorie::create([
            'kategorie_id' => 3,
            'display_name' => 'Trenýrky',
            'name' => 'trenyrky'
            ]);
    }
}
