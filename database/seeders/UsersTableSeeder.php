<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $moderatorRole = Role::where('name', 'moderator')->first();
        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();
        

        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@admin.com',
            'password' => Hash::make('asdasdasd')
        ]);
        $moderator = User::create([
            'name' => 'Moderator User',
            'email' => 'moderator@moderator.com',
            'password' => Hash::make('asdasdasd')
        ]);
       
        $user = User::create([
            'name' => 'Normal User',
            'email' => 'user@user.com',
            'password' => Hash::make('asdasdasd')
        ]);

        $admin->roles()->attach($adminRole);
        $moderator->roles()->attach($moderatorRole);
        $user->roles()->attach($userRole);
    }
}
