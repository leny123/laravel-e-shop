<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjednavkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objednavkas', function (Blueprint $table) {
            $table->id();
            $table->string('objednavka_id');
            $table->integer('status')->default('0');
            $table->integer('total');
            $table->string('user_email');
            $table->string('user_jmeno');
            $table->string('user_prijmeni');
            $table->string('user_mesto');
            $table->string('user_ulice');
            $table->string('user_psc');
            $table->integer('user_telefon');
            $table->string('preprava');
            $table->string('platba');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objednavkas');
    }
}
