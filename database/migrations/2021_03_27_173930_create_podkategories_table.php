<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodkategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podkategories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('kategorie_id');
            $table->string('display_name');
            $table->string('name');
            $table->foreign('kategorie_id')->references('id')->on('kategorie')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podkategories');
    }
}
