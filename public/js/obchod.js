/******/ (() => { // webpackBootstrap
/*!********************************!*\
  !*** ./resources/js/obchod.js ***!
  \********************************/
var toggleShopMenuButton = document.querySelector('#shopMenuButton');
var obchodMenu = document.querySelector('.obchod-menu-content');
toggleShopMenuButton.addEventListener('click', function () {
  if (obchodMenu.style.visibility === 'visible') {
    obchodMenu.style.visibility = 'hidden';
  } else {
    obchodMenu.style.visibility = 'visible';
  }
});

window.onresize = function (event) {
  if (window.innerWidth >= 991) {
    obchodMenu.style.visibility = 'visible';
  } else {
    obchodMenu.style.visibility = 'hidden';
  }
};
/******/ })()
;