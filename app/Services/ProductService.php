<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\Kategorie;
use App\Models\Podkategorie;

class ProductService
{
    public function filterProducts($urls, $products, $znacka, $filter = null)
    {
     
        ($urls['kategorie'] != 'none') 
         ? $kategorie = Kategorie::where('name', $urls['kategorie'])->first() 
         : '';

        ($urls['podkategorie'] != 'none') 
         ? $podkategorie = Podkategorie::where('name', $urls['podkategorie'])->first() 
         : '';
         
        if (isset($podkategorie)) {
              ($urls['gender'] != 'none') 
              ? $products = $podkategorie->products()->where('gender', $urls['gender'])->get()
              : $products = $podkategorie->products()->get();

        } elseif (isset($kategorie)) {
             ($urls['gender'] != 'none') 
             ? $products = $kategorie->products()->where('gender', $urls['gender'])->get()
             : $products = $kategorie->products()->get();

        } else {
            ($urls['gender'] != 'none')
             ? $products = Product::where('gender', $urls['gender'])->get()
             : $products = Product::all();

        }
        ($znacka == 'Vše') ? : $products = $products->where('znacka', $znacka);  
        foreach ($products as $product) {
            $idcka[] = $product->id;
        }
        
        if(!isset($idcka)) {
            return;
        }

        if ($filter == 'lowest') {
            $products = Product::whereIn('id', $idcka)->orderBy('cena')->paginate('27');
        } elseif ($filter == 'highest') {
            $products = Product::whereIn('id', $idcka)->orderByDesc('cena')->paginate('27');
        } elseif ($filter == 'sleva') {
            $products = Product::whereIn('id', $idcka)->where('sleva', '!=', 0)->orderBy('sleva')->paginate('27');
        } else {
            $products = Product::whereIn('id', $idcka)->paginate('27');
        }
      
        return $products;
    }

    public function featuredProducts($product, $featuredPodkategorie = null)
    {
        $featuredProducts = [];

        if ($featuredPodkategorie == null) {
            $products = [];
            $productIds = [];
        } else {
            $products = $featuredPodkategorie->first()->products->take(10);
            foreach ($products as $featuredProduct) {
                array_push($featuredProducts, $featuredProduct);
                $productIds[] = $featuredProduct->id;
            }
        }
           
        if (count($products) < 10) {
            $kategorie = $product->kategorie->first();
            $featuredKategories = $kategorie->products->where('id', '!=', $productIds)->take(10-count($products));

            foreach ($featuredKategories as $featuredKategorie) {
                array_push($featuredProducts, $featuredKategorie);
            }
        }
        if (count($featuredProducts) < 10) {
            $otherProducts = Product::where('id', '!=', $featuredProducts)->get()->take(10 - count($featuredProducts));

            foreach ($otherProducts as $otherProduct) {
                array_push($featuredProducts, $otherProduct);
            }
        }
        
        return $featuredProducts;
    }

    public function getProductStock($prodsizes, $sizes) {
        foreach ($sizes as $size) {
            $velikosti[$size->sizes] = [
                'stock' => 0,
                'size' => $size->sizes,
            ];
        }
        foreach ($prodsizes as $prodsize) {
            $itemy = array('stock' => $prodsize->pivot->stock);
            $velikosti[$prodsize->sizes] = array_replace($velikosti[$prodsize->sizes], $itemy);
        }
        return $velikosti;
    }

    public function getFinalPhoto($req, $fotka, $productFotka) {
        if ($fotka == "") {
            $fotkaFinalName = $productFotka;

            return $fotkaFinalName;
        } else {
            Storage::disk('public')->delete($productFotka);

            $fotkaFinalName = time() . '-' . $req->file('fotka')->getClientOriginalName();
            $fotka->move(public_path('storage'), $fotkaFinalName);

            return $fotkaFinalName;
        }
        
    }
}
