<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\Session;

class KosikService
{
    public function kosikTotal()
    {
        $products = $this->getProducts();
        $kosik = Session::get('kosik');
        $payment = Session::get('payment');
        $total = 0;
        if ($products) {
            foreach ($products as $product) {
                ($product->sleva == 0) ? $cena = $product->cena : $cena = $product->sleva;
                
                $total = $total + ($cena * $kosik[$product->id]['pocet']);
            }
            if ($payment) {
                ($payment['preprava'] == "ppl") ? $preprava = 79 : $preprava = 59;
                ($payment['platba'] == "dobirka") ? $platba = 19 : $platba = 0;

                $total = $total + $platba + $preprava;
            }
            return $total;
        }
    }

    public function storeProducts($id, $size, $pocet = 1)
    {
        $kosik = Session::get('kosik');
            if (!$kosik) {
                $array[$id] = [
                    'id' => $id,
                    'size' => $size,
                    'pocet' => $pocet
                ];
                return $array;
            } else {
                $array = [
                    'id' => $id,
                    'size' => $size,
                    'pocet' => $pocet
                ];
                $kosik[$id] = $array;
                return $kosik;
            }
        
        if (is_integer($id) && count($size) != 0) {
            if (!$kosik) {
                $array[$id] = [
                    'id' => $id,
                    'size' => $size->first()->sizes,
                    'pocet' => 1
                ];
                return $array;
            } else {
                $array = [
                    'id' => $id,
                    'size' => $size->first()->sizes,
                    'pocet' => 1
                ];
                $kosik[$id] = $array;
                return $kosik;
            }
        } else {
            return 1;
        }
    }


    public function getProducts()
    {
        $kosikSession = Session::get('kosik');
        if (isset($kosikSession) && !empty($kosikSession)) {
            foreach ($kosikSession as $id => $kosikProduct) {
                $product = Product::with(['sizes'])->find($id);
                (!isset($product))
                 ? Session::pull('kosik.' . $id)
                 : $products[] = $product;
            }
            return $products;
        } else {
            return 0;
        }
    }
}
