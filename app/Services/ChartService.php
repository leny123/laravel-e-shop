<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\Session;

class ChartService
{
   public function getProductBoughtChart($boughtProducts) {
      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '01') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $ledenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '02') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $unorBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '03') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $brezenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '04') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $dubenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '05') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $kvetenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '06') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $cervenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '07') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $cervenecBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '08') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $srpenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '09') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $zariBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '10') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $rijenBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '11') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         }
      }
      $listopadBoughtProducts[] = $pocet;

      $pocet = 0;
      foreach($boughtProducts as $boughtProduct) {
         if($boughtProduct->pivot->created_at->format('m') == '12') {
            $pocet += $boughtProduct->products()->first()->pivot->pocet;
         } 
      }
      $prosinecBoughtProducts[] = $pocet;

     $vysledek = [
      $ledenBoughtProducts[0],
      $unorBoughtProducts[0],
      $brezenBoughtProducts[0],
      $dubenBoughtProducts[0],
      $kvetenBoughtProducts[0],
      $cervenBoughtProducts[0],
      $cervenecBoughtProducts[0],
      $srpenBoughtProducts[0],
      $zariBoughtProducts[0],
      $rijenBoughtProducts[0],
      $listopadBoughtProducts[0],
      $prosinecBoughtProducts[0]
     ];
    return $vysledek;
   }

   public function getProductStockChart($product) {
      if(count($product->sizes) == 0) {
         return false; 
      }
         foreach($product->sizes as $productSize) {
            $sizes[] = $productSize->sizes;
            $stock[] = $productSize->pivot->stock;
         }
         $sizeStock = [];
         array_push($sizeStock, $sizes);
         array_push($sizeStock, $stock);
         return $sizeStock;
   }
}


