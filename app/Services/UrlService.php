<?php

namespace App\Services;
use Illuminate\Support\Facades\Route;

class UrlService
{
    public function getUrlGender() {
        switch (Route::current()->getName()) {
            case ('home'): 
                return 'home';
                break;
            case ('muzi.index'): 
                return 'm';
                break;
            case ('zeny.index'): 
                return 'f';
                break;
            case ('muzi.kategorie'): 
                return 'm';
                break;
            case ('zeny.kategorie'): 
                return 'f';
                break;
            case ('muzi.podkategorie'): 
                return 'm';
                break;
            case ('zeny.podkategorie'): 
                return 'f';
                break;
            default: 
            return 'none';
            break;
        }
    }
}
