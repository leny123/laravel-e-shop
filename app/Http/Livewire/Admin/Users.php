<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\User;

class Users extends Component
{
    public $searchInput;
    public $users;

    public function mount() {
        $roleName = 'user';
        $this->users = User::whereHas('roles', function ($query) use ($roleName) {
            $query->where('name', '!=', $roleName);
        })->get();
    }

    public function render()
    {
        return view('livewire.admin.users');
    }

    public function search() {
        $this->users = User::where('email', 'like', "%$this->searchInput%")->get();
    }
}
