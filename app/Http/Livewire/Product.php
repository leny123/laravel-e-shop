<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Services\KosikService;
use Illuminate\Support\Facades\Session;
use App\Models\Rating;
use Illuminate\Support\Facades\Gate;

class Product extends Component
{
    public $product;
    public $size;
    public $pocet = 1;
    public $stock;
    public $message = "";
    public $averageStars = 0;
    public $paginatePocet = 5;
    public $boughtProduct;

    protected $listeners = [
        'updateRating' => 'render'
    ];
    
    public function mount()
    {
        if (count($this->product->sizes) != 0) {
            $this->size = $this->product->sizes->first()->sizes;
            $this->stock = $this->product->sizes->where('sizes', $this->size)->first()->pivot->stock;
        }
        ($this->stock < 5) ? $this->message = "Zbývá méně než 5 produktů!" : '';
    }

    public function render()
    {
        (isset($this->product->sizes->where('sizes', $this->size)->first()->pivot->stock)) 
         ? $this->stock = $this->product->sizes->where('sizes', $this->size)->first()->pivot->stock 
         : '';
     
        if (!isset($this->stock)) {
            $this->message = "Produkt je momentálně nedostupný";
        } elseif ($this->stock < 5) {
            $this->message = "Zbývá méně než 5 produktů!";
        } else {
            $this->message = "";
        }

        $ratings = $this->product->rating;
        foreach($ratings as $rating) {
            $stars[] = $rating->status;
        }
        $ratingCount = count($ratings);
        if($ratingCount != 0) {
            $this->averageStars = array_sum($stars) / $ratingCount;
            $this->averageStars = number_format($this->averageStars, 1);
        }
        $ratings = $ratings->take($this->paginatePocet);
      
        return view('livewire.product', [
            'ratings' => $ratings,
            'ratingCount' => $ratingCount
        ]);
    }

    public function store()
    {
        ($this->pocet < 1) ? $this->pocet = 1 : '';
        $kosikContent = (new KosikService())->storeProducts($this->product->id, $this->size, $this->pocet);
        
        if ($kosikContent == 1 || $this->pocet > $this->stock) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Snažíte se objednat více produktů než máme na skladě.',
                'type' => 'error'
            ]);
        } else {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Produkt úspěšně přidán.',
                'type' => 'success'
            ]);
            Session::put('kosik', $kosikContent);
            $this->emit('updateCount');
        }
    }

    public function loginFailAlert($alert) {
        if($alert == 0) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Pro vytvoření recenze se musíte přihlásit.',
                'type' => 'error'
            ]);
        } elseif($alert == 1) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Nemůžete dělat recenzi na produkt, který jste si nezakoupili.',
                'type' => 'error'
            ]);
        } elseif($alert == 2) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Na tento produkt již máte recenzi.',
                'type' => 'error'
            ]);
        }
    }

    public function loadMore() 
    {
        $this->paginatePocet += 5;
    }

    public function destroyRating(Rating $rating) {
        if (Gate::denies('manage-shop')) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Nemáš na tohle právo.',
                'type' => 'error'
            ]);
        } else {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Recenze smazána.',
                'type' => 'success'
            ]);
            $rating->user()->detach($rating->user()->first());
            $rating->delete();
            $this->emit('updateRating');
        }
    }
}
