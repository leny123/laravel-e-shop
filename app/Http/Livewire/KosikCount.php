<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Session;
use Livewire\Component;

class KosikCount extends Component
{
    protected $listeners =['updateCount' => 'render'];

    public function render()
    {
        $sessionKosik = Session::get('kosik');
        return view('livewire.kosik-count', [
            'kosikContent' => $sessionKosik
        ]);
    }
}
