<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Znacka;
use App\Services\KosikService;
use App\Services\ProductService;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithPagination;

class Products extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $filter;
    public $znacka = 'Vše';
    public $urls;
    public $search;

    public function mount() {
        $this->resetPage();
    }
    
    public function render()
    {
        ($this->search) 
         ? $products = Product::where('name', 'like', "%$this->search%")->paginate('27')
         : $products = 1;
         
        (is_integer($products))
         ? $products = (new ProductService())->filterProducts($this->urls, $products, $this->znacka, $this->filter)
         : '';

        $znacky = Znacka::all();
        return view('livewire.products', [
            'products' => $products,
            'znacky' => $znacky
        ]);
    }

    public function store(Product $product)
    {
        (count($product->sizes) == 0) 
         ? $size = 1  
         : $size = $product->sizes->first()->sizes;
        ($size == 1) 
         ? $kosikContent = 1 
         : $kosikContent = (new KosikService())->storeProducts($product->id, $size);

        if ($kosikContent == 1) {
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Tenhle produkt je vyprodaný',
                'type' => 'error'
            ]);
        } else {
            Session::put('kosik', $kosikContent);
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Produkt úspěšně přidán.',
                'type' => 'success'
            ]);
            $this->emit('updateCount');
        }
    }
    public function removeKosikProduct($id) {
        Session::pull('kosik.' . $id);
        $this->dispatchBrowserEvent('swal:title-alert', [
            'title' => 'Produkt úspěšně odebrán.',
            'type' => 'success'
        ]);
        $this->emit('updateCount');
    }
    public function destroy($id)
    {
        Product::destroy($id);
    }
    public function FilterByPrice($filter)
    {
        $this->resetPage();
        $this->filter = $filter;
    }
}
