<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Services\KosikService;
use Illuminate\Support\Facades\Session;

class KosikMenu extends Component
{
    protected $listeners = ['updateKosik' => 'render'];
    public function render()
    {
        $paymentSession = Session::get('payment');
        $kosikSession = Session::get('kosik');
        $products = (new KosikService())->getProducts();
        $total = (new KosikService())->kosikTotal();
        return view('livewire.kosik-menu', [
            'products' => $products,
            'payment' => $paymentSession,
            'kosikSession' => $kosikSession,
            'total' => $total,
        ]);
    }
}
