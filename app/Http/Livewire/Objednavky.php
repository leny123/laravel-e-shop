<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Objednavka;

class Objednavky extends Component
{
    use WithPagination;
    public $paginatePocet = 5;
    public $paginateRender = true;
    public $objednavkaStatus = 0;
    public $objednavkaId;
    public $vyhledatInput;

    public function render()
    {
        if (!$this->vyhledatInput) {
            ($this->objednavkaStatus == 1) 
             ? $objednavky = Objednavka::with(['products'])
               ->where('status', 1)
               ->orderBy('updated_at', 'DESC')
               ->paginate($this->paginatePocet)
             : $objednavky = Objednavka::with(['products'])
               ->where('status', 0)
               ->orderBy('created_at', 'ASC')
               ->paginate($this->paginatePocet);
        } else {
            $objednavky = Objednavka::with(['products'])
            ->where('objednavka_id', $this->vyhledatInput)
            ->orWhere('user_email', 'like', "%$this->vyhledatInput%")
            ->paginate($this->paginatePocet);
        }
       
        (count($objednavky) < $this->paginatePocet) 
         ? $this->paginateRender = false 
         : $this->paginateRender = true;
        $vyrizene = Objednavka::where('status', 1)->count();
        $nevyrizene = Objednavka::where('status', 0)->count();
       
        return view('livewire.objednavky', [
            'objednavky' => $objednavky,
            'vyrizene' => $vyrizene,
            'nevyrizene' => $nevyrizene,

        ]);
    }
    public function vyrizene()
    {
        $this->objednavkaStatus = 1;
        $this->paginatePocet = 5;
    }
    public function nevyrizene()
    {
        $this->objednavkaStatus = 0;
        $this->paginatePocet = 5;
    }

    public function destroy($id)
    {
        $this->dispatchBrowserEvent('swal:title-alert', [
            'title' => 'Objednávka zamítnuta',
            'type' => 'error'
        ]);
        Objednavka::destroy($id);
    }

    public function accept($id)
    {
        $objednavka = Objednavka::where('id', $id)->first();
        $objednavka->status = 1;
        $objednavka->save();
        $this->dispatchBrowserEvent('swal:title-alert', [
            'title' => 'Objednávka příjmuta',
            'type' => 'success'
        ]);
    }
    public function loadMore()
    {
        $this->paginatePocet += 5;
    }
}
