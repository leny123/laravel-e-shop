<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Rating;

class PridatRecenzi extends Component
{
    public $rating = 1;
    public $display_name;
    public $text;
    public $product;
    public $stars = 1;

    public function render()
    {
        return view('livewire.pridat-recenzi');
    }
    public function pridatRecenzi() {
        if(Auth::check()) {
            $rating = Rating::create([
                'display_name' => $this->display_name,
                'rating' => $this->rating,
                'status' => $this->stars,
                'text' => $this->text,
                ]);
            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Recenze úspěšně přidána.',
                'type' => 'success'
            ]);
            $rating->product()->attach(
                [
                    $this->product->id => [
                        'user_id' => Auth::user()->id
                    ]
                ]
            );
            $this->emit('updateRating'); 
        }
    }
    
}
