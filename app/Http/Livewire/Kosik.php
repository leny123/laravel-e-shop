<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use App\Services\KosikService;

class Kosik extends Component
{
    public $pocet = array();
    public $size = array();
    public $checkstock = false;
    
    public function mount()
    {
        $kosikSession = Session::get('kosik');
        $products = (new KosikService())->getProducts();
       
        if ($products != 0) {
            foreach ($products as $product) {
                $this->size[$product->id] = $kosikSession[$product->id]['size'];
                $this->pocet[$product->id] = $kosikSession[$product->id]['pocet'];
            }
        }
    }
    public function render()
    {
        $kosikSession = Session::get('kosik');
        $products = (new KosikService())->getProducts();
        return view('livewire.kosik', [
            'products' => $products,
            'kosikSession' => $kosikSession
        ]);
    }

    public function destroy($id)
    {
        $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Produkt úspěšně odebrán.',
                'type' => 'success'
            ]);
        Session::pull('kosik.' . $id);
        $this->emit('updateKosik');
        $this->emit('updateCount');
    }
    public function sizeChange($id)
    {
        $kosikSession = Session::get('kosik');
        $product = Product::find($id);
        $productStock = $product->sizes->where('sizes', $this->size[$id])->first()->pivot->stock;
    
        if ($productStock < $this->pocet[$id]) {
            $this->pocet[$id] = $productStock;
            $kosikSession = Session::get('kosik');
            $kosikSession[$id]['size'] = $this->size[$id];
            Session::put('kosik', $kosikSession);

            $this->dispatchBrowserEvent('swal:title-alert', [
                'title' => 'Snažíte se objednat více kusů než momentálně máme na skladě.',
                'type' => 'error'
            ]);
            
        } else {
            $kosikSession = Session::get('kosik');
            $kosikSession[$id]['size'] = $this->size[$id];
            Session::put('kosik', $kosikSession);
        }
        $this->pocetChange($id);
    }
    public function pocetChange($id)
    {
        $kosikSession = Session::get('kosik');
        $product = Product::find($id);
        $productStock = $product->sizes->where('sizes', $kosikSession[$product->id]['size'])->first()->pivot->stock;

        if ($productStock < $this->pocet[$id]) {
            $this->pocet[$id] = $productStock;
            $this->dispatchBrowserEvent('swal:title-alert', [
                    'title' => 'Snažíte se objednat více kusů než momentálně máme na skladě.',
                    'type' => 'error'
                ]);
        } else {
            ($this->pocet[$id] < 1) ? $this->pocet[$id] = 1 : '';
            $kosikSession[$id]['pocet'] = $this->pocet[$id];
            Session::put('kosik', $kosikSession);
            
            $this->emit('updateKosik');
        }
    }
}
