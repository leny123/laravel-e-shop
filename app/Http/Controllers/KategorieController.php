<?php

namespace App\Http\Controllers;

use App\Http\Requests\pridatKategoriiRequest;
use Illuminate\Http\Request;
use App\Models\Kategorie;

class KategorieController extends Controller
{
    public function save(pridatKategoriiRequest $req)
    {
        Kategorie::create($req->validated());
        return back();
    }
    public function delete(Request $req)
    {
        Kategorie::destroy($req->id);
        return back();
    }
}
