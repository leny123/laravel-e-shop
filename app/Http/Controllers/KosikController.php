<?php

namespace App\Http\Controllers;

use App\Http\Requests\checkoutRequest;
use App\Models\Size;
use App\Models\Objednavka;
use Illuminate\Support\Facades\Session;
use App\Services\KosikService;
use Illuminate\Support\Facades\Auth;

class KosikController extends Controller
{

    // ukázat košík
    public function show()
    {
        $total = (new KosikService())->kosikTotal();
        $products = (new KosikService())->getProducts();
        $sizes = Size::all();
        $paymentSession = Session::get('payment');
        $paymentSession ?: $paymentSession = 1;
        $kosikContent = Session::get('kosik');

        if ($products) {
            return view('obchod.kosik.kosik', [
                'products' => $products,
                'kosikSession' => $kosikContent,
                'sizes' => $sizes,
                'payment' => $paymentSession,
                'total' => $total
            ]);
        } else {
            return view('obchod.kosik.kosik');
        }
    }

    //ukázat checkout
    public function checkout()
    {
        $total = (new KosikService())->kosikTotal();
        $products = (new KosikService())->getProducts();
        $kosikContent = Session::get('kosik');
        $paymentSession = Session::get('payment');
        $paymentSession ?: $paymentSession = 1;

        if ($products) {
            return view('obchod.kosik.checkout', [
                'checkoutitems' => $kosikContent,
                'prods' => $products,
                'payment' => $paymentSession,
                'total' => $total
            ]);
        } else {
            return view('obchod.kosik.checkout', [
                'checkoutitems' => $kosikContent
            ]);
        }
    }

    public function payment(checkoutRequest $req)
    {
        $req->validated();
        Session::put('payment', $req->except('_token'));
        return $this->paymentGET();
    }

    public function paymentGET()
    {
        $total = (new KosikService())->kosikTotal();
        $products = (new KosikService())->getProducts();
        $paymentSession = Session::get('payment');
        $kosikSession = Session::get('kosik');

        if (isset($paymentSession)) {
            return view('obchod.kosik.payment', [
                'kosikSession' => $kosikSession,
                'paymentSession' => $paymentSession,
                'products' => $products,
                'total' => $total
            ]);
        } else {
            return redirect('/kosik/checkout');
        }
    }

    public function finishPayment()
    {
        $kosikSession = Session::get('kosik');
        $products = (new KosikService())->getProducts();
        $paymentSession = Session::get('payment');

        foreach ($products as $product) {
            $size = $product->sizes()->where('sizes', $kosikSession[$product->id]['size'])->first();
            $vysledek = $size->pivot->stock - $kosikSession[$product->id]['pocet'];
            if ($vysledek < 0) {
                return back()->withErrors('Snažíte se objednat: ' . $product->name . ' ale takový počet nemáme momentálně na skladě.');
            }
        }
        foreach ($products as $product) {
            $size = $product->sizes()->where('sizes', $kosikSession[$product->id]['size'])->first();
            $vysledek = $size->pivot->stock - $kosikSession[$product->id]['pocet'];
            $product->bought += $kosikSession[$product->id]['pocet'];
            $product->sizes()->sync(
                [
                    $size->id => [
                        'stock' => $vysledek
                    ]
                ],
                false
            );
        }

        $objednavka = Objednavka::create([
            'objednavka_id' => sha1(time()),
            'total' => (new KosikService())->kosikTotal(),
            'user_email' => $paymentSession['email'],
            'user_jmeno' => $paymentSession['jmeno'],
            'user_prijmeni' => $paymentSession['prijmeni'],
            'user_mesto' => $paymentSession['mesto'],
            'user_ulice' => $paymentSession['ulice'],
            'user_psc' => $paymentSession['psc'],
            'user_telefon' => $paymentSession['telefon'],
            'preprava' => $paymentSession['preprava'],
            'platba' => $paymentSession['platba'],
        ]);

        foreach ($products as $product) {
            $objednavka->products()->attach([
                $product->id => [
                    'pocet' => $kosikSession[$product->id]['pocet'],
                    'size' => $kosikSession[$product->id]['size'],
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        }

        if(Auth::check()) {
            foreach($products as $product) {
                Auth::User()->products()->attach($product);
            }
        }
        Session::forget('kosik');
        return redirect('kosik/success');
    }
}
