<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Objednavka;

class ObjednavkyController extends Controller
{
    public function show()
    {
        $objednavky = Objednavka::orderBy('created_at', 'ASC')->get();

        return view('admin.objednavky.index', [
            'objednavky' => $objednavky
        ]);
    }
}
