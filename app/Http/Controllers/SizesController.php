<?php

namespace App\Http\Controllers;
use App\Models\Size;
use Illuminate\Http\Request;

class SizesController extends Controller
{
    public function store(Request $req) {
        Size::create(['sizes' => $req->jmeno]);
        return back();
    }
    
    public function destroy(Request $req) {
        Size::destroy($req->id);
        return back();
    }
}
