<?php

namespace App\Http\Controllers;

use App\Http\Requests\pridatProductRequest;
use App\Http\Requests\EditProductRequest;
use App\Models\Product;
use App\Models\Kategorie;
use App\Models\Size;
use App\Models\Znacka;
use App\Models\Podkategorie;
use App\Services\ChartService;
use App\Services\ProductService;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Illuminate\Support\Facades\Gate;

class ProductsController extends Controller
{
    public function show($id)
    {
        $product = Product::find($id);
        $podkategorie = Podkategorie::all();
        
        if(count($product->kategorie) == 0) {
            $featuredProducts = false;
        } elseif (isset($product->podkategorie->first()->name)) {
            $featuredPodkategorie = $podkategorie->where('name', $product->podkategorie->first()->name);
            $featuredProducts = (new ProductService())->featuredProducts($product, $featuredPodkategorie);
        } else {
            $featuredProducts = (new ProductService())->featuredProducts($product);
        }
    
        if (Gate::denies('manage-shop')) {
            return view('obchod.product', [
                'product' => $product,
                'featuredProducts' => $featuredProducts,
            ]);
        } else {
            $lists = Kategorie::all();
            $sizes = Size::all();
            $znacky = Znacka::all();
        }

        $velikosti = (new ProductService())->getProductStock($product->sizes, $sizes);
   
        return view('obchod.product', [
            'podkategorie' => $podkategorie,
            'product' => $product,
            'featuredProducts' => $featuredProducts,
            'lists' => $lists,
            'sizes' => $sizes,
            'velikosti' => $velikosti,
            'znacky' => $znacky
        ]);
    }

    public function showStats($product) {
        $product = Product::find($product);
        if(!$product) {
            return back();
        }
        $yearlyBoughtProducts = (new ChartService())->getProductBoughtChart($product->objednavky);
        $productStock = (new ChartService())->getProductStockChart($product);
        if($productStock) {
            $stockChart = (new LarapexChart)->barChart()
            ->setTitle('Počet velikostí skladem.')
            ->addData('Počet', $productStock[1])
            ->setXAxis($productStock[0])
            ->setGrid()
            ->setMarkers(['#FF5722', '#E040FB'], 7, 10);
        } else {
            $stockChart = false;
        }
        
        $chart = (new LarapexChart)->areaChart()
            ->setTitle('Prodej produktu za celý rok.')
            ->addData('Počet', $yearlyBoughtProducts)
            ->setColors(['#05f529', '#ff6384'])
            ->setXAxis(['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec' ])
            ->setGrid()
            ->setMarkers(['#FF5722', '#E040FB'], 7, 10);

        return view('productGraph', [
            'graph' => $chart,
            'stockChart' => $stockChart,
            'product' => $product
        ]);
    }

    public function save(pridatProductRequest $req)
    {
        $fotkaFinalName = time() . '-' . $req->file('fotka')->getClientOriginalName();
        $req->validated();
        $req->fotka->move(public_path('storage'), $fotkaFinalName);
        $kategorie = Kategorie::where('display_name', $req->kategorie)->first();
        
        $product = Product::create([
            'name' => $req->name,
            'fotka' => $fotkaFinalName,
            'popis' => $req->popis,
            'cena' => $req->cena,
            'gender' => $req->gender,
            'znacka' => $req->znacka,
        ]);
        $product->kategorie()->sync($kategorie);
        return redirect(url('product/' . $product->id));
    }
  
    public function update(EditProductRequest $req)
    {
        $req->validated();
        $product = Product::find($req->id);
        $podkategorie = Podkategorie::where('name', $req->podkategorie)->first();
        $kategorie = Kategorie::where('name', $req->kategorie)->first();
        $upravenaFotka = (new ProductService())->getFinalPhoto($req, $req->fotka, $product->fotka);
        (isset($req->size)) ? $sizes = Size::wherein('sizes', $req->size)->get() : '';

        $product->update([
            'name' => $req->name,
            'fotka' => $upravenaFotka,
            'popis' => $req->popis,
            'cena' => $req->cena,
            'sleva' => $req->sleva,
            'gender' => $req->gender,
            'znacka' => $req->znacka,
        ]);
        
        $product->podkategorie()->detach();
        $product->kategorie()->detach();
        $podkategorie->products()->attach($product);
        $kategorie->products()->attach($product);
        $product->sizes()->detach();
        if (isset($sizes)) {
            foreach ($sizes as $i => $size) {
                $size->products()->attach([
                    $product->id => [
                        'stock' => $req->stock[$i]
                    ]
                ]);
            }
        }
        return redirect(url('product/' . $req->id));
    }
}
