<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Znacka;

class ZnackyController extends Controller
{
    public function store(Request $req)
    {
        Znacka::create(['display_name' => $req->jmeno]);
        return back();
    }

    public function destroy(Request $req)
    {
        Znacka::destroy($req->id);
        return back();
    }
}
