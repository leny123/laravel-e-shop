<?php

namespace App\Http\Controllers;

use App\Models\Kategorie;
use App\Models\Size;
use App\Models\Znacka;
use App\Models\Podkategorie;
use App\Services\UrlService;

class ShopController extends Controller
{
    public function showIndex()
    {
        $podkategorie = Podkategorie::all();
        $lists = Kategorie::all();
        $sizes = Size::all();
        $znacky = Znacka::all();
        $urls = [
            'gender' => 'none',
            'kategorie' => 'none',
            'podkategorie' => 'none',
        ];

        if ((new UrlService())->getUrlGender() == 'home') {
            return view('home', [
                'podkategorie' => $podkategorie,
                'lists' => $lists,
                'sizes' => $sizes,
                'znacky' => $znacky,
                'urls' => $urls
            ]);
        } else {
            $urls['gender'] = (new UrlService())->getUrlGender();
        }
        return view('obchod.index', [
            'podkategorie' => $podkategorie,
            'lists' => $lists,
            'sizes' => $sizes,
            'znacky' => $znacky,
            'urls' => $urls
        ]);
    }

    public function showKategorie($name)
    {
        $podkategorie = Podkategorie::all();
        $kategorie = Kategorie::all();
        $sizes = Size::all();
        $znacky = Znacka::all();
        $urls = [
            'gender' => (new UrlService())->getUrlGender(),
            'kategorie' => $name,
            'podkategorie' => 'none'
        ];
        return view('obchod.index', [
            'podkategorie' => $podkategorie,
            'lists' => $kategorie,
            'sizes' => $sizes,
            'znacky' => $znacky,
            'urls' => $urls
        ]);
    }

    public function showPodkategorie($kategorieUrl, $podkategorieUrl)
    {
        $sizes = Size::all();
        $znacky = Znacka::all();
        $lists = Kategorie::all();
        $urls = [
            'gender' => (new UrlService())->getUrlGender(),
            'kategorie' => $kategorieUrl,
            'podkategorie' => $podkategorieUrl
        ];

        return view('obchod.index', [
            'podkategorie' => $podkategorieUrl,
            'lists' => $lists,
            'sizes' => $sizes,
            'znacky' => $znacky,
            'urls' => $urls
        ]);
    }
}
