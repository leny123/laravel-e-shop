<?php

namespace App\Http\Controllers;

use App\Http\Requests\pridatPodkategoriiRequest;
use Illuminate\Http\Request;
use App\Models\Podkategorie;

class PodkategorieController extends Controller
{
    public function save(pridatPodkategoriiRequest $req)
    {
        Podkategorie::create($req->validated());
        return back();
    }
    public function delete(Request $req)
    {
        Podkategorie::destroy($req->id);
        return redirect(route('home'));
    }
}
