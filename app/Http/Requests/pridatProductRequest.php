<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class pridatProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'fotka' => 'required|mimes:jpg,jpeg,png|max:10000',
            'popis' => 'required',
            'cena' => 'required|integer',
            'gender' => 'required',
            'kategorie' => 'required',
            'znacka' => 'required',
        ];
        
    }
}
