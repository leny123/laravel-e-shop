<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class checkoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'jmeno' => 'required',
            'prijmeni' => 'required',
            'mesto' => 'required',
            'ulice' => 'required',
            'psc' => 'required',
            'telefon' => 'required|integer',
            'preprava' => 'required',
            'platba' => 'required',
            
        ];
    }
}
