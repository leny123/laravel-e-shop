<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategorie extends Model
{
    use HasFactory;

    protected $table = 'kategorie';
    
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'display_name'
    ];

    public function podkategorie() {
        return $this->hasMany(Podkategorie::class);
    }
    public function products() {
        return $this->belongsToMany(Product::class);
    }
}
