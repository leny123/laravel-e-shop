<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use HasFactory;

    protected $table = 'sizes';
    protected $primaryKey = 'id';
    protected $fillable = ['sizes'];

    public $timestamps = false;

    public function Products() {
        return $this->belongsToMany(Product::class)
        ->withPivot(['stock']);
        
    }
}

