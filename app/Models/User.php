<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }

    public function hasAnyRoles($roles)
    {
        if ($this->roles()->whereIn('name', $roles)->first()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function purchasedProduct($product) {
        if($this->products->where('id', $product)->first()) {
            return true;
        } else {
            return false;
        }
    }

    public function hasRating($product) {
        if($this->rating()->wherePivot('product_id', $product->id)->first()) {
            return false;
        } else {
            return true;
        }
    }

    public function product() {
        return $this->BelongsToMany(Product::class, 'product_rating_user')->withPivot('rating_id');
    }

    public function rating() {
        return $this->BelongsToMany(Rating::class, 'product_rating_user')->withPivot('product_id');
    }
}
