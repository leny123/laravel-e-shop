<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rating extends Model
{
    use HasFactory;

    protected $table = 'ratings';
    protected $primaryKey = 'id';

    protected $fillable = [
        'display_name',
        'user_id',
        'rating',
        'status',
        'text'
    ];

    public function product() {
        return $this->BelongsToMany(Product::class, 'product_rating_user')->withPivot('user_id');
    }

    public function user() {
        return $this->BelongsToMany(User::class, 'product_rating_user')->withPivot('product_id');
    }
}
