<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Podkategorie extends Model
{
    use HasFactory;

    protected $table = 'podkategories';
    
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'display_name',
        'kategorie_id',
    ];
    
    public function shopList() {
       return $this->belongsTo(Kategorie::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }
}
