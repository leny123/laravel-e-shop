<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objednavka extends Model
{
    use HasFactory;

    protected $table = 'objednavkas';
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'objednavka_id',
        'total',
        'user_email',
        'user_jmeno',
        'user_prijmeni',
        'user_mesto',
        'user_ulice',
        'user_psc',
        'user_telefon',
        'preprava',
        'platba'
    ];
    
    public function products() {
        return $this->BelongsToMany(Product::class)
        ->withPivot(['pocet', 'size'])
        ->withTimeStamps();
    }
}
