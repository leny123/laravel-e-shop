<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Znacka extends Model
{
    use HasFactory;

    protected $table = 'znacky';
    protected $primaryKey = 'id';
    protected $fillable = ['display_name'];

    public $timestamps = false;
}
