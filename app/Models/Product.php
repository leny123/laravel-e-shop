<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $table = 'products';
    
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'gender',
        'kategorie',
        'znacka',
        'sleva',
        'fotka',
        'cena',
        'popis',
    ];

    public function Sizes() {
        return $this->belongsToMany(Size::class)->withPivot(['stock']);
    }

    public function podkategorie() {
        return $this->belongsToMany(Podkategorie::class);
    }

    public function objednavky() {
        return $this->belongsToMany(Objednavka::class)
        ->withPivot(['pocet', 'size'])
        ->withTimeStamps();
    }

    public function kategorie() {
        return $this->belongsToMany(Kategorie::class);
    }

    public function users() {
        return $this->BelongsToMany(Product::class);
    }
    
    public function user() {
        return $this->BelongsToMany(Product::class, 'product_rating_user')->withPivot('rating_id');
    }

    public function rating() {
        return $this->BelongsToMany(Rating::class, 'product_rating_user')->withPivot('user_id');
    }
    
}
