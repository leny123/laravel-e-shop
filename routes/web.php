<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ShopController::class, 'showIndex'])->name('home');
Route::get('product/{id}',[ProductsController::class, 'show'])->name('product');
Route::get('product/{id}/statistiky',[ProductsController::class, 'showStats'])->name('product.stats')->middleware('can:manage-objednavky');


Route::prefix('obchod')->group(function() {
    Route::get('/muzi', [ShopController::class, 'showIndex'])->name('muzi.index');
    Route::get('/zeny', [ShopController::class, 'showIndex'])->name('zeny.index');
    
    Route::get('/muzi/{name}', [ShopController::class, 'showKategorie'])->name('muzi.kategorie');
    Route::get('/zeny/{name}', [ShopController::class, 'showKategorie'])->name('zeny.kategorie');;
    Route::get('/{name}', [ShopController::class, 'showKategorie'])->name('obchod.kategorie');;
    
    Route::get('/muzi/{name}/{podkategorie}', [ShopController::class, 'showPodkategorie'])->name('muzi.podkategorie');
    Route::get('/zeny/{name}/{podkategorie}', [ShopController::class, 'showPodkategorie'])->name('zeny.podkategorie');;
    Route::get('/{name}/{podkategorie}', [ShopController::class, 'showPodkategorie'])->name('obchod.podkategorie');;
});

Auth::routes();

//KOSIK

Route::prefix('kosik')->name('kosik.')->group(function () {
    Route::get('/', [KosikController::class, 'show'])->name('show');
    Route::get('/checkout', [KosikController::class, 'checkout'])->name('checkout');
    Route::post('/payment', [KosikController::class, 'payment'])->name('payment');
    Route::get('/payment', [KosikController::class, 'paymentGET'])->name('paymentGET');
    Route::post('/finishPayment', [KosikController::class, 'finishPayment'])->name('finishpayment');
    Route::view('/success', 'obchod.kosik.success');
});

//MODERATOR
Route::group(['middleware' => 'auth'], function() {
    Route::get('admin/objednavky', [Admin\ObjednavkyController::class, 'show'])->name('admin.objednavky')->middleware('can:manage-objednavky');

//ADMIN
    Route::group(['middleware' => 'can:manage-shop'], function() {
       
        Route::prefix('obchod')->group(function () {
            Route::post('/pridatProdukt', [ProductsController::class, 'save'])->name('product.pridat');
            Route::put('/product/editProdukt',[ProductsController::class, 'update'])->name('product.update');
        
            Route::post('/pridatKategorie', [KategorieController::class, 'save'])->name('kategorie.pridat');
            Route::post('/odebratKategorie', [KategorieController::class, 'delete'])->name('kategorie.odebrat');
        
            Route::post('/odebratPodkategorie', [PodkategorieController::class, 'delete'])->name('podkategorie.odebrat');
            Route::post('/pridatPodkategorie', [PodkategorieController::class, 'save'])->name('podkategorie.pridat');
        
            Route::post('/pridatSize', [SizesController::class, 'store'])->name('size.pridat');
            Route::post('/odebratSize', [SizesController::class, 'destroy'])->name('size.odebrat');
        
            Route::post('/pridatZnacka', [ZnackyController::class, 'store'])->name('znacka.pridat');
            Route::post('/odebratZnacka', [ZnackyController::class, 'destroy'])->name('znacka.odebrat');
        });
    });
});

//PERMISSIONS
Route::prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function() {
    Route::resource('/users', Admin\UsersController::class, ['except' => ['show', 'create', 'store']]);
});

