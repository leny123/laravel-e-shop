/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { size } = require('lodash');

require('./bootstrap');
require('summernote');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: '#app',
});
window.addEventListener('load', function () {
/*   updateCart(); */
  updateBox();
});

// VARIABLES

$(document).ready(function() {
  $('#popis').summernote({
    height: 150,
    codemirror: {
      theme: 'ttcn'
    }
  });

  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:20,
    nav:true,
    navText: ["<div class='nav-button owl-prev'>‹</div>", "<div class='nav-button owl-next'>›</div>"],
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
  })
});

var boxy = document.querySelectorAll('#sizecheck');
var stocks = document.querySelectorAll('#stockinput');


$('.checkboxpreprava').on('change', function() {
  $('.checkboxpreprava').not(this).prop('checked', false);  
  if($(this).prop('checked', true)) {
    $(this).prop('checked', true);
  }
});
$('.checkboxplatba').on('change', function() {
  $('.checkboxplatba').not(this).prop('checked', false);  
  if($(this).prop('checked', true)) {
    $(this).prop('checked', true);
  }
});




// FOR LOOPS EVENT LISTENERS




for (var i = 0; i < boxy.length; i++) {
  box = boxy[i]
  box.addEventListener('change', updateBox)
}


for (var i = 0; i < stocks.length; i++) {
  stock = stocks[i]
  stock.addEventListener('change', updateStock)
}

// FUNKCE

function updateStock() {
  for (var i = 0; i < stocks.length; i++) {
    stock = stocks[i]
    if (stock.value < 0) {
      stock.value = 0;
    }
  }

}
function updateBox() {
  for (var i = 0; i < stocks.length; i++) {
    box = boxy[i]
    stock = stocks[i]
    if (box.checked) {
      stock.style.visibility = "visible"
      stock.setAttribute("name", "stock[]")
    } else {
      stock.style.visibility = "hidden"
      stock.setAttribute("name", " ")
    }
  }
}









