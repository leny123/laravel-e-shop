@extends('layouts.app')
@section('content')
    <div class="container">
        <h1 class="text-center my-3">Statistiky</h1>
        <div class="row justify-content-center">
            <div class="text-right" style="width: 800px;">
                <a href="{{ route('product', [$product->id]) }}">
                    <button class="btn btn-secondary">Zpět</button>
                </a>
            </div>
        </div>
        <h2 class="text-center my-3"></h2>
        <div class="row justify-content-center my-5">
            <div class="text-center border" style="width: 800px;">
                {{ $graph->container() }}
            </div>
        </div>
        @if ($stockChart)
            <div class="row justify-content-center my-5">
                <div class="text-center border" style="width: 800px;">
                    {{ $stockChart->container() }}
                </div>
            </div>
        @endif
    </div>
@endsection