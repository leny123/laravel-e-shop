@section('productmenu')
    <div class="obchod-menu">
        <button id="shopMenuButton" style="background:none;border:none;">
            <i class="fas fa-bars text-white obchod-menu-icon"></i>
        </button>
        <div class="obchod-menu-content">
            <div class="obchod-menu-black">
            <h3 class="text-center py-2 text-white">Obchod
                <span class=" 
                    @if ($urls['gender']=='f' ) 
                        zenymenu 
                    @elseif($urls['gender']=='m' ) 
                        muzimenu 
                    @endif 
                ">
                    @if ($urls['gender']=='f' ) 
                        - Ženy
                    @elseif($urls['gender']=='m' ) 
                        - Muži
                    @endif
                </span>
            </h3 class="text-center">
            <hr class="hr">
            <ul class="">
                <a href="
                    @if ($urls['gender'] == 'f')     
                        {{ url('obchod/zeny') }}
                    @elseif ($urls['gender'] == 'm') 
                        {{ url('obchod/muzi') }}
                    @else                            
                        {{ url('/') }}
                    @endif  
                ">   
                    <li class="text-left py-2 px-3 text-white
                        @if (Request::is('obchod/muzi')  || Request::is('obchod/zeny')  || Request::is('/') ) bg-lightgrey @endif">
                        Vše
                    </li>
                </a>

                @foreach ($lists as $list)
                    <a href=" 
                        @if ($urls['gender'] == 'f' )    
                            {{ url('obchod/zeny/' . $list->name) }}
                        @elseif ($urls['gender'] == 'm') 
                            {{ url('obchod/muzi/' . $list->name) }}
                        @else                            
                            {{ url('/obchod/' . $list->name) }} 
                        @endif
                    ">
                        <li class="text-left d-flex py-2 px-3 position-relative @if (str_contains(url()->current(), $list->name)) bg-lightgrey @endif">
                            <p class="text-white my-auto">{{ $list->display_name }}</p>
                                @can('manage-shop')
                                    <form action="{{ route('kategorie.odebrat') }}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{ $list->id }}" name="id">
                                        <button type="submit" class="trashbtn my-1">
                                            <i class="fas fa-trash  m-2"></i>
                                        </button>
                                    </form>
                                @endcan
                        </li>
                    </a>
        
                    @if (str_contains(url()->current(), $list->name))
                        @foreach ($list->podkategorie as $podkategorie)
                            <a href="
                                @if ($urls['gender']=='f' )      
                                    {{ url('obchod/zeny/' . $list->name) . '/' . $podkategorie->name }}
                                @elseif ($urls['gender'] == 'm') 
                                    {{ url('obchod/muzi/' . $list->name . '/' . $podkategorie->name) }}
                                @else                            
                                    {{ url('/obchod/' . $list->name) . '/' . $podkategorie->name }} 
                                @endif 
                            ">
                                <li class=" text-left py-2 my-1 px-5 podkategorie text-white 
                                    {{ (str_contains(url()->current(), $podkategorie->name)) ? 'bg-grey' : '' }}
                                ">
                                    - {{ $podkategorie->display_name }}
                                        @can('manage-shop')
                                            <form action="{{ route('podkategorie.odebrat') }}" method="POST">
                                                @csrf
                                                <input type="hidden" value="{{ $podkategorie->id }}" name="id">
                                                <button type="submit" class="trashbtn my-1">
                                                    <i class="fas fa-trash  m-2"></i>
                                                </button>
                                            </form>
                                        @endcan
                                </li>
                            </a>
                        @endforeach
                    @endif
                @endforeach

                @can('manage-shop')
                    <li class="text-center admin py-2">
                        <button data-toggle="modal" data-target="#AdminListAddModal"
                            class="btn btn-outline-success ">
                            <i class="fas fa-plus"></i>
                        </button>
                    </li>
                @endcan

            </ul>
        </div>
            @can('manage-shop')
                @yield('adminmenu')
            @endcan
        </div>
    </div>
@endsection