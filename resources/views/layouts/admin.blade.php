@section('adminmenu')
<div class="admin-menu my-2 ">
    <h3 class="text-center py-2">Admin Menu</h3>
    <hr>
    <ul>
        <li class=" text-center text-white py-2 position-relative d-flex mx-2 justify-content-center">
            <h4 class="py-2">Produkt</h4>
            <button data-toggle="modal" data-target="#adminAddProduct" class="btn btn-outline-success admin-menu-btn">
                <i class="fas fa-plus"></i>
            </button>     
        </li>

        <li class="text-center text-white py-2 position-relative d-flex mx-2 justify-content-center">
            <h4 class="py-2">Velikost</h4>
            <button data-toggle="modal" data-target="#adminAddSize" class="btn btn-outline-success admin-menu-btn">
                <i class="fas fa-plus"></i>
            </button>
        </li>

        <li class="text-center text-white py-2 position-relative d-flex mx-2 justify-content-center">
            <h4 class="py-2">Značka</h4>
            <button data-toggle="modal" data-target="#adminAddZnacka" class="btn btn-outline-success admin-menu-btn">
                <i class="fas fa-plus"></i>
            </button>    
        </li>
    </ul>
</div>
@endsection