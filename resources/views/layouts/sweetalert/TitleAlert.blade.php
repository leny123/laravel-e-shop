<template id="title-alert">
      <swal-param name="allowEscapeKey" value="false" />
      <swal-param name="timer" value="5000" />
      <swal-param name="toast" value="true" />
      <swal-param name="width" value="300px" />
      <swal-param name="height" value="300px" />
      <swal-param name="timerProgressBar" value="true" />
      <swal-param name="position" value="top-end" />
      <swal-param name="showConfirmButton" value="false" />
      <swal-param
        name="customClass"
        value='{ "popup": "my-popup" }' />
</template>