    @section('AdminEditProduktModal')
        <div class="modal fade large-modal " id="adminEditProduct" tabindex="-1" role="dialog"
            aria-labelledby="adminEditProductTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-center modal-bg ">
                    <div class="modal-header text-center">
                        <h5 class="modal-title mx-auto">Upravit Produkt.</h5>
                    </div>
                    <form action="{{ route('product.update') }}" method="POST"  enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="modal-body text-center">
                            <div class="row justify-content-center">
                                <div class="col m-2">
                                    <h4 class="my-1">Jméno produktu</h4>
                                    <input class="admin-modal-input" value="{{ $product->name }}" type="text" name="name">
                                    <input type="hidden" name="id" value="{{ $product->id }}">

                                    <h4 class="my-1">Cena Produktu</h4>
                                    <input class="admin-modal-input" value="{{ $product->cena }}" type="text" name="cena">

                                    <h4 class="my-1">Sleva 
                                        <p class="font-little">(Zadejte novou částku pro produkt 0 = žádná sleva)</p>
                                    </h4>
                                    <input class="admin-modal-input" value="{{ $product->sleva }}" type="text" name="sleva">

                                    <h4 class="my-1">Fotka</h4>
                                    <input value="" type="file" name="fotka">
                                </div>

                                <div class="col m-2">
                                    <h4>Kategorie</h4>
                                    <select class="my-2" name="kategorie" id="kategorie">
                                        @foreach ($lists as $list)
                                            <option value="{{ $list->name }}" 
                                                {{ ($list->name == $product->kategorie) ? 'selected' : '' }}
                                            >
                                                {{ $list->display_name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <select class="my-2" name="podkategorie">
                                        @foreach ($podkategorie as $pod)
                                            <option value="{{ $pod->name }}" 
                                                @isset($product->podkategorie()->first()->name)
                                                    {{ ($pod->name == $product->podkategorie()->first()->name) ? 'selected' : '' }}
                                                @endisset
                                            >
                                                {{ $pod->display_name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <h4>Pohlaví</h4>
                                    <select class="my-2" name="gender" id="gender">
                                        @if ($product->gender == 'm')
                                            <option value="m">Muž</option>
                                            <option value="f">Žena</option>
                                        @else
                                            <option value="f">Žena</option>
                                            <option value="m">Muž</option>
                                        @endif
                                    </select>

                                    <h4>Značka</h4>
                                    <select name="znacka" class="my-2">
                                        @foreach ($znacky as $znacka)
                                            <option value="{{ $znacka->display_name }}" 
                                                {{ ($znacka->display_name == $product->znacka) ? 'selected' : '' }}
                                            >
                                                {{ $znacka->display_name }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <h4>Velikosti</h4>
                                    <div class="form-check text-left">
                                        @foreach ($sizes as $size)
                                            <br>
                                            <input id="sizecheck" value="{{ $size->sizes }}" type="checkbox" name="size[]" 
                                                @foreach ($product->sizes as $prodsize) 
                                                    {{ ($prodsize->sizes == $size->sizes) ? 'checked' : '' }}
                                                @endforeach
                                            >
                                            <label>{{ $size->sizes }}</label>
                                            <input id="stockinput" type="number" name="stock[]" style="width: 6vh;"
                                                value="{{ $velikosti[$size->sizes]['stock'] }}"
                                            >
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                           <h4>Popis produktu</h4>
                           <textarea name="popis" id="popis">{{ $product->popis }}</textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary py-2 d-block" data-dismiss="modal">Zavřít</button>
                            <button type="submit" class="btn btn-primary py-2 d-block">Upravit Produkt</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection
