@section('AdminPridatProduktModal')
    <div class="modal fade large-modal" id="adminAddProduct" tabindex="-1" role="dialog" aria-labelledby="adminAddProductTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content text-center modal-bg">
                <div class="modal-header text-center">
                    <h5 class="modal-title mx-auto">Přidat Produkt.</h5>
                </div>
                <form action="{{ route('product.pridat') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body text-center">
                        <div class="row justify-content-center">
                            <div class="d-block mx-2">
                                <h4 class="">Jméno produktu</h4>
                                @error('name') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p>
                                @enderror
                                <input type="text" name="name" class="admin-modal-input">
                            </div>

                            <div class="d-block mx-2">
                                <h4>Cena Produktu</h4>
                                @error('cena') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                                <input type="text" name="cena" class="admin-modal-input">
                            </div>
                        </div>

                        <div class="row justify-content-center my-3">
                            <div class="d-block mx-2">
                                <h4 class="my-2">Kategorie</h4>
                                <select name="kategorie" id="AdminPridatProductKategorie">
                                    @foreach ($lists as $list)
                                        <option value="{{ $list->name }}">{{ $list->display_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="d-block mx-2">
                                <h4 class="my-2">Pohlaví</h4>
                                <select name="gender" id="gender">
                                    <option value="m">Muž</option>
                                    <option value="f">Žena</option>
                                </select>
                            </div>

                            <div class="d-block mx-2">
                                <h4 class="my-2">Značka</h4>
                                <select name="znacka">
                                    @foreach ($znacky as $znacka)
                                        <option value="{{ $znacka->display_name }}">{{ $znacka->display_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="d-block mx-2 my-3 justify-content-center">
                            <h4>Fotka</h4>
                            @error('fotka') 
                                <p class=" mx-2 text-danger">{{ $message }}</p> 
                            @enderror
                            <input type="file" name="fotka">
                        </div>

                        <div class="d-flex justify-content-center">
                            <h4>Popis produktu</h4>
                            @error('popis') 
                                <p class=" mx-2 text-danger">{{ $message }}</p> 
                            @enderror
                        </div>
                        <textarea id="popis" name="popis" cols="80" rows="3"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary py-2 d-block" data-dismiss="modal">
                            Zavřít
                        </button>
                        <button type="submit" class="btn btn-primary py-2 d-block">
                            Přidat Produkt
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
