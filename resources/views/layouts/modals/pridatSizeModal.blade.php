@section('AdminPridatSizeModal')
    <div class="modal fade" id="adminAddSize" tabindex="-1" role="dialog" aria-labelledby="AdminAddSizeTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content text-center modal-bg">
                <div class="modal-header text-center">
                    <h5 class="modal-title mx-auto">Přidat Velikost do seznamu.</h5>
                </div>
                <form action="{{ route('size.pridat') }}" method="POST">
                    @csrf

                    <div class="modal-body">
                        <h4 class="py-2">Jméno nové velikosti.</h4>
                        <input class="admin-modal-input" type="text" name="jmeno">
                        <button type="submit" class="btn btn-primary mx-2 ">Přidat Velikost</button>
                        <div class="row justify-content-center my-2">
                            <h1>Seznam všech velikostí</h1>

                            <div class="col-6">
                                @foreach ($sizes as $size)
                                    <div class="velikost d-flex justify-content-center">
                                        <input type="hidden" name="id" value="{{ $size->id }}">
                                        <button type="submit" class="trashbtn-velikost m-1">
                                            <i class="fas fa-trash font-medium m-2"></i>
                                        </button>
                                        <h3 class="py-4">{{ $size->sizes }}</h3>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary " data-dismiss="modal">
                Zavřít
            </button>
        </div>
    </div>
@endsection