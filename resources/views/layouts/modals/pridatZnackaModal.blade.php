@section('AdminPridatZnackaModal')
    <div class="modal fade" id="adminAddZnacka" tabindex="-1" role="dialog" aria-labelledby="AdminAddZnackaTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content text-center modal-bg">
                <div class="modal-header text-center">
                    <h5 class="modal-title mx-auto">Přidat Velikost do seznamu.</h5>
                </div>
                <form action="{{ route('znacka.pridat') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <h4 class="py-2">Jméno nové Značky.</h4>
                        <input class="admin-modal-input" type="text" name="jmeno">
                        <button type="submit" class="btn btn-primary mx-2 ">Přidat Značku</button>
                        <div class="row justify-content-center my-2">
                </form>
                        <h1>Seznam všech značek</h1>
                            <div class="col-6">
                                @foreach ($znacky as $znacka)
                                    <form action="{{ route('znacka.odebrat') }}" method="POST">
                                        @csrf
                                        <div class="velikost d-flex justify-content-center">
                                            <input type="hidden" name="id" value="{{ $znacka->id }}">
                                            <button type="submit" class="trashbtn-velikost m-1">
                                                <i class="fas fa-trash font-medium m-2"></i>
                                            </button>
                                            <h3 class="py-4">{{ $znacka->display_name }} </h3>
                                        </div>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">
                        Zavřít
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection