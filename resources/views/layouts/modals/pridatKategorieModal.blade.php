@if (!request()->is('/admin/*'))
    @section('AdminPridatListModal')
        <div class="modal fade" id="AdminListAddModal" tabindex="-1" role="dialog" aria-labelledby="AdminListAddModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered " role="document">
                <div class="modal-content text-center modal-bg">
                    <div class="modal-header text-center">
                        <h5 class="modal-title mx-auto">Přidat Kategorie</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6 mt-auto">
                                <form action="{{ route('kategorie.pridat') }}" method="POST">
                                    @csrf

                                    <h4 class="py-2">Jméno nové Kategorie.</h4>
                                    <input type="text" name="display_name">

                                    <h4 class="py-2">Jméno bez diakritiky, bez mezer</h4>
                                    <input type="text" name="name">
                                    <button type="submit" class="btn btn-primary my-2">Přidat Kategorii</button>
                                </form>
                            </div>
                            <div class="col-6">
                                <form action="{{ route('podkategorie.pridat') }}" method="POST">
                                    @csrf

                                    <h4 class="py-2">Vyberte kategorii</h4>
                                    <select name="kategorie_id">
                                        @foreach ($lists as $list)
                                            <option value="{{ $list->id }}">{{ $list->display_name }}</option>
                                        @endforeach
                                    </select>

                                    <h4 class="py-2">Jméno nové Podkategorie.</h4>
                                    <input type="text" name="display_name">

                                    <h4 class="py-2">Jméno bez diakritiky, bez mezer</h4>
                                    <input type="text" name="name">
                                    <button type="submit" class="btn btn-primary my-2">Přidat Podkategorii</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                    </div>
                </div>
            </div>
        </div>
    @endsection
@endif