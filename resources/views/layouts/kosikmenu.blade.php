@section('kosikmenu')

<div class="kosikmenu">
    <h2 class="text-center text-white py-2 ">Celková cena</h2>
    <hr class="hr">
    @isset($products)
        @foreach ($products as $i => $prod)
            <div class="d-flex ">
                @if ($prod->sleva == 0)
                    <p class="  text-white mr-auto mx-2 my-1">{{ $prod->name }} </p>
                    <span class="text-white  ml-auto mx-2 my-1"> 
                        {{ $kosikSession[$prod->id]['pocet'] }} x {{ $prod->cena }} Kč
                    </span>
                @else
                    <p class=" text-white  mr-auto mx-2 my-1">{{ $prod->name }} </p>
                    <span class="text-white ml-auto mx-2 my-1">
                        {{ $kosikSession[$prod->id]['pocet'] }} x {{ $prod->sleva }} Kč
                    </span>
                @endif
            </div>
        @endforeach

        @if ($payment != 1)
            <div class="d-flex ">
                <p class=" text-white mr-auto mx-2 my-1">Doprava </p>
                <p class=" text-white ml-auto mx-2 my-1">
                    @if($payment['preprava'] == "ppl")
                        79 Kč
                    @else
                        59 Kč
                    @endif
                </p>
            </div>
            <div class="d-flex ">
                <p class=" text-white mr-auto mx-2 my-1">Platba</p>
                <p class=" text-white ml-auto mx-2 my-1">
                    @if ($payment['platba'] == 'karta' || $payment['platba'] == 'banka')
                        Zdarma 
                    @else
                        19 Kč
                    @endif
                </p>
            </div>
        @endif

        <hr class="hr">
        <h2 class="text-center text-white pb-2">{{ $total }} Kč</h2>

        @if (Request::is('kosik'))
            <div class="tlacitko text-center py-2">
                <a href="{{ route('kosik.checkout') }}">
                    <button type="submit" class="btn btn-outline-success">
                        Přejít do pokladny
                    </button>
                </a>
            </div>
        @endif
    @endisset
</div>

@endsection