@extends('layouts.app')

@can('manage-shop')
    @include('layouts.modals.editProductModal')
@endcan

@include('layouts.sweetalert.TitleAlert')

@section('content')
            <div class="container mt-5">
                @livewire('product', ['product' => $product])
            <div class="row">
                <div class="col">
                    <h1 class="text-center mb-4">Podobné produkty</h1>
                    @if($featuredProducts)
                        <div class="owl-carousel owl-theme position-relative mb-4">
                            @foreach ($featuredProducts as $featuredProduct)
                                <a href="{{ url('product/' . $featuredProduct->id) }}">
                                    <div class="carousel-product">
                                        <div class="carousel-product-fotka">
                                            <img width="100%;" height="100%;" src="
                                                @if ($featuredProduct->fotka == "none")
                                                    {{ asset('images/product-image-placeholder.jpg') }}
                                                @else
                                                    {{ asset('storage/' . $featuredProduct->fotka) }}
                                                @endif
                                                "
                                            >
                                        </div>

                                        <div class="carousel-product-name d-flex justify-content-center">
                                            <h5 class="my-auto text-center text-dark">{{ $featuredProduct->name }}</h5>
                                        </div>

                                        <div class="carousel-product-cena  d-flex justify-content-center">
                                            @if ($featuredProduct->sleva == 0)
                                                <h3 class="my-auto text-dark">{{ $featuredProduct->cena }}Kč</h3>
                                            @else
                                                <h3 class="my-auto text-danger">{{ $featuredProduct->sleva }}Kč</h3>
                                            @endif
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    @else 
                        @can('manage-shop')
                            <h1 class="text-danger text-center">
                                Tenhle produkt nemá nastavenou žádnou kategorii.
                                Nastavte kategorii pro zobrazování doporučených produktů
                            </h1>
                        @endcan
                    @endif
                </div>
            </div>
        </div>
    @can('manage-shop')
        @yield('AdminEditProduktModal')
    @endcan
@endsection
