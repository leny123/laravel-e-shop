@extends('layouts.app')

@can('manage-shop')
    @include('layouts.modals.pridatProduktModal')
    @include('layouts.modals.pridatKategorieModal')
    @include('layouts.modals.pridatSizeModal')
    @include('layouts.modals.pridatZnackaModal')
    @include('layouts.admin')
@endcan

@include('layouts.sweetalert.TitleAlert')
@include('layouts.productMenu')

@section('content')
        <div class="container mt-5">
            <h1 class=" text-center font-big text-black">
                Obchod 
                <span class=" {{ ($urls['gender'] == 'f') ? 'zenymenu' : 'muzimenu' }} ">
                    @if ($urls['gender']=='f' )      
                        - Ženy
                    @elseif ($urls['gender'] == 'm') 
                        - Muži
                    @endif
                </span>
            </h1>
        <hr class="hr my-3">
            <div class="row">
                <div class="col-3 menucol d-md">
                    <div class="sticky-col">
                        @yield('productmenu')
                    </div>
                </div>
                <div class="col productscol">
                    @livewire('products', ['urls' => $urls])
                </div>
            </div>
        </div>
    @can('manage-shop')
        @yield('AdminPridatListModal')
        @yield('AdminPridatProduktModal')
        @yield('AdminPridatSizeModal')
        @yield('AdminPridatZnackaModal')
    @endcan
@endsection
