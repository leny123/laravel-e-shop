@extends('layouts.app')

@section('content')
    <div class="container">
        @if (isset($products) && $products)
            <h1 class="text-center pt-2">Souhrn</h1>
            @if ($errors->any())
                <h5 class="text-danger text-center">{{ $errors->first() }}</h5>
            @endif
            <hr class="hr">
            <div class="row my-5">
                <div class="col-lg-4 mb-2 osobniudaje">
                    <h1 class="py-2 ">Osobní údaje</h1>
                    
                    <p>E-Mail:<span class="text-primary"> {{ $paymentSession['email'] }}</span></p>
                    <p>Jméno:<span class="text-primary"> {{ $paymentSession['jmeno'] }}</span></p>
                    <p>Příjmení:<span class="text-primary"> {{ $paymentSession['prijmeni'] }}</span></p>
                    <p>Město:<span class="text-primary"> {{ $paymentSession['mesto'] }}</span></p>
                    <p>Ulice:<span class="text-primary"> {{ $paymentSession['ulice'] }}</span></p>
                    <p>PSČ:<span class="text-primary"> {{ $paymentSession['psc'] }}</span></p>
                    <p>Telefonní číslo:<span class="text-primary"> {{ $paymentSession['telefon'] }}</span></p>
                </div>
                <div class="col-lg-4 mb-2">
                    <h1 class="text-center py-2">Platba & Doprava</h1>
                    <p class="text-center">Doprava:<span class="text-primary"> {{ $paymentSession['preprava'] }}</span></p>
                    <p class="text-center">Platba:<span class="text-primary"> {{ $paymentSession['platba'] }}</span></p>
                </div>
                <div class="col-lg-4 mb-2 ">
                    <h1 class="text-center py-2">Košík</h1>
                    <div class="row text-center" style="height:2vh;">
                        <div class="payment-product-jmeno"><p>Jméno</p></div>
                        <div class="payment-product-size"><p>Velikost</p></div>
                        <div class="payment-product-pocet"> <p>Počet</p></div>
                        <div class="payment-product-cena d-flex"><p>Cena</p></div>
                    </div>
                    <hr class="hr my-2">
                    @foreach ($products as $product)
                        <div class="row text-center">
                            <div class="payment-product-jmeno">
                                <p class="px-2">{{ $product->name }}</p>
                            </div>

                            <div class="payment-product-size">
                                <p class="px-2">{{ $kosikSession[$product->id]['size'] }}</p>
                            </div>

                            <div class="payment-product-pocet">
                                <p class="px-2">{{ $kosikSession[$product->id]['pocet'] }}</p>
                            </div>

                            <div class="payment-product-cena d-flex">
                                @if($product->sleva == 0)
                                    <p>{{ $product->cena }} Kč</p>
                                @else
                                    <del><p>{{ $product->cena }} Kč</p></del>
                                    <p class="text-danger  px-2">{{ $product->sleva }} Kč </p>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    <div class="row text-center">
                        <div class="payment-product-jmeno">
                            <p class="px-2">Doprava</p>
                        </div>
                        <div class="payment-product-cena d-flex ml-auto">
                            @if($paymentSession['preprava'] == "ppl") 
                                <p class="px-2">79 Kč</p>
                            @else
                                <p class="px-2">59 Kč</p>
                            @endif
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="payment-product-jmeno">
                            <p class="px-2">Platba</p>
                        </div>
                        <div class="payment-product-cena d-flex ml-auto">
                            @if($paymentSession['platba'] == "karta" || $paymentSession['platba'] == "banka") 
                                <p class="px-2">Zdarma</p>
                            @else
                                <p class="px-2">19 Kč</p>
                            @endif
                        </div>
                    </div>
                    <hr class="hr my-2">
                    <h3 class="text-primary text-center ">{{ $total }} Kč</h3>
                </div>
            </div>
            <hr class="hr my-2">
            <form action="{{ route('kosik.finishpayment') }}" method="post">
                @csrf
                <div class="row justify-content-center py-2">
                    <button type="submit" class="btn btn-success">Dokončit objednávku</button>
                </div>
            </form>
        @else
            <h1>Košík je prázdný</h1>
        @endif
    </div>
@endsection
