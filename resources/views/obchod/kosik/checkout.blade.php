@extends('layouts.app')
@include('layouts.kosikmenu')
@section('content')
    <div class="container">
        @if (isset($checkoutitems) && $checkoutitems)
            <div class="row ">
                <div class="col my-5 checkoutcol">
                    <h1 class=" py-2">Adresa</h1>
                    <div class="adress-form ">
                        <form action="{{ route('kosik.payment') }}" method="POST" id="adressform" name="adressform">
                            @csrf

                            <div class=" d-flex">
                                <h5 class="">E-mailová adresa</h5>
                                @error('email') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                            </div>
                            <input name="email" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['email'] }} @else {{ old('email') }} @endif">

                            <div class="d-flex">
                                <h5 class="">Křestní jméno</h5>
                                @error('jmeno') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                            </div>
                            <input name="jmeno" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['jmeno'] }} @else {{ old('jmeno') }} @endif">

                            <div class="d-flex">
                                <h5 class="">Příjmení</h5>
                                @error('prijmeni') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <input name="prijmeni" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['prijmeni'] }} @else {{ old('prijmeni') }} @endif">

                            <div class="d-flex">
                                <h5 class="">Město</h5>
                                @error('mesto') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <input name="mesto" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['mesto'] }} @else {{ old('mesto') }} @endif">

                            <div class="d-flex">
                                <h5 class="">Ulice</h5>
                                @error('ulice') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                            </div>
                            <input name="ulice" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['ulice'] }} @else {{ old('ulice') }} @endif">

                            <div class="d-flex">
                                <h5 class="">PSČ</h5>
                                @error('psc') 
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                            </div>
                            <input name="psc" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['psc'] }} @else {{ old('psc') }} @endif">

                            <div class="d-flex">
                                <h5 class="">Telefonní číslo</h5>
                                @error('telefon')
                                    <p class=" mx-2 text-danger">{{ $message }}</p> 
                                @enderror
                            </div>
                            <input name="telefon" type="text" class="mb-2 w-100" value="@if ($payment !=1) {{ $payment['telefon'] }} @else {{ old('telefon') }} @endif">

                            <h1 class=" py-2 ">Způsob dopravy</h1>
                            <div class="preprava-group my-4 d-flex">
                                <div class="preprava-checkbox ">
                                    <input name="preprava" class="checkboxpreprava" type="checkbox" id="ppl" value="ppl"
                                        {{ ($payment != 1 && $payment['preprava'] == 'ppl') ? 'checked' : '' }}
                                        {{ (empty($payment['preprava']) ? 'checked' : '') }}
                                    >
                                    <label for="ppl">PPL (79Kč)</label>
                                </div>

                                <div class="preprava-fotka text-right">
                                    <img src="{{ url('images/ppl.jpg') }}" style="width:10vh;">
                                </div>
                            </div>

                            <div class="preprava-group my-4 d-flex">
                                <div class="preprava-checkbox ">
                                    <input id="ceskaposta" class="checkboxpreprava" name="preprava"  type="checkbox" value="ceskaposta"
                                    {{ ($payment != 1 && $payment['preprava'] == 'ceskaposta') ? 'checked' : '' }}
                                    >
                                    <label for="ceskaposta">Česká pošta (59Kč)</label>
                                </div>

                                <div class="preprava-fotka text-right">
                                    <img src="{{ url('images/ceskaposta.png') }}" style="width:10vh;">
                                </div>
                            </div>

                            <h1 class=" py-2 ">Platební metody</h1>
                            <div class="preprava-group my-4 d-flex">
                                <input id="karta" class="checkboxplatba" name="platba" type="checkbox" value="karta" 
                                {{ ($payment != 1 && $payment['platba'] == 'karta') ? 'checked' : '' }}
                                {{ (empty($payment['platba']) ? 'checked' : '') }}
                                >
                                <label for="karta">Platební karta (Zdarma)</label>
                            </div>

                            <div class="preprava-group my-4 d-flex">
                                <input id="banka" class="checkboxplatba" name="platba" type="checkbox" value="banka" 
                                {{ ($payment != 1 && $payment['platba'] == 'banka') ? 'checked' : '' }}
                                >
                                <label for="banka">Bankovní převod (Zdarma)</label>
                            </div>

                            <div class="preprava-group my-4 d-flex">
                                <input id="dobirka" class="checkboxplatba" name="platba" type="checkbox" value="dobirka"
                                {{ ($payment != 1 && $payment['platba'] == 'dobirka') ? 'checked' : '' }}
                                >
                                <label for="dobirka">Dobírka (19Kč)</label>
                            </div>
                        </form>
                        <div class="buttonlist d-flex mr-auto py-5">
                            <a href="{{ url('kosik') }}">
                                <button class="btn btn-primary ">Zpět</button>
                            </a>
                            <input class="btn btn-success ml-auto" type="submit"  form="adressform" value="Pokračovat">
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 ">
                    <div class="sticky-col ">
                        @livewire('kosik-menu')
                    </div>
                </div>
            </div>
        @else
            <h1>Košík je prázdný</h1>
        @endif
    </div>
@endsection
