@extends('layouts.app')

@section('content')
        <div class="container text-center">
            <h1 class="text-center mt-3">Objednávka byla úspěšně odeslána.</h1>
            <h2 class="text-center">Na E-mail jsme vám zaslali více informací.</h2>
            <a href="/">
                <button class="btn btn-success my-2">Zpět do obchodu</button>
            </a>
        </div>
@endsection
