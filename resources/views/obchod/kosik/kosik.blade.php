@extends('layouts.app')
@include('layouts.kosikmenu')
@include('layouts.sweetalert.TitleAlert')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="text-center py-5">Košík</h1>
                    @livewire('kosik')
                </div>

                <div class="col-xl-3">
                    @livewire('kosik-menu')
                </div>
            </div>
        </div>
@endsection
