<div>
    <div class="kosikmenu mx-auto my-5">
        <h2 class="text-center text-white py-2 ">Celková cena</h2>
        <hr class="hr">
        @isset($products)
            @if (!empty($products))
                @foreach ($products as $i => $product)
                    <div class="d-flex">
                        <div class="w-50" style="overflow-wrap: break-word;">
                            <p class="text-white mx-2 my-1">{{ $product->name }}</p>
                        </div>
                        <span class="text-white  ml-auto mx-2 my-auto"> {{ $kosikSession[$product->id]['pocet'] }} x
                        @if ($product->sleva == 0)
                            {{ $product->cena }} 
                        @else 
                            {{ $product->sleva }}
                        @endif 
                        Kč
                        </span>
                    </div>
                @endforeach

                @isset($payment)
                    <div class="d-flex">
                        <p class=" text-white mr-auto mx-2 my-1">Doprava </p>
                        <p class=" text-white ml-auto mx-2 my-1">
                            @if ($payment['preprava'] == 'ppl')
                                79 Kč
                            @else
                                59 Kč
                            @endif
                        </p>
                    </div>

                    <div class="d-flex">
                        <p class=" text-white mr-auto mx-2 my-1">Platba</p>
                        <p class=" text-white ml-auto mx-2 my-1">
                        @if ($payment['platba'] == 'karta' || $payment['platba'] == 'banka')
                            Zdarma 
                        @else 
                            19 Kč
                        @endif
                        </p>
                    </div>
                @endisset

                <hr class="hr">
                <h2 class="text-center text-white pb-2">{{ $total }} Kč</h2>
                <div class="tlacitko text-center py-2">
                    @if (!Request::is('kosik/checkout'))
                        <a href="{{ route('kosik.checkout') }}">
                            <button type="submit" class="btn btn-outline-success">
                                Přejít do pokladny
                            </button>
                        </a>
                    @else
                        <a href="{{ url('kosik') }}">
                            <button type="submit" class="btn btn-outline-success">
                                Přejít do košíku
                            </button>
                        </a>
                    @endif
                </div>
            @endif
        @endisset
    </div>
</div>