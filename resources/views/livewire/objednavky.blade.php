<div>
    <h1 class="text-center mt-5">Objednávky</h1>
    <div class="row justify-content-center my-3">
        <input type="text" class="success" style="width:30vw;" placeholder="Vyhledat objednávku"
               wire:model="vyhledatInput">
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="row justify-content-center">
                <button class="btn {{ $objednavkaStatus == 0 ? 'btn-danger' : 'btn-outline-danger' }} mx-3" 
                        wire:click="nevyrizene">
                    <b>Zobrazit Nevyřízené</b> ({{ $nevyrizene }})
                </button>

                <button class="btn {{ $objednavkaStatus === 1 ? 'btn-success' : 'btn-outline-success ' }} mx-3" 
                        wire:click="vyrizene">
                    <b>Zobrazit Vyřízené</b> ({{ $vyrizene }})
                </button>
            </div>
            @foreach ($objednavky as $objednavka)
                <div class="card objednavky-card my-2">
                    <h1 class="text-white"></h1>
                    <div class="card-header d-flex">
                        <h5 class="text-white">Objednávka:</h5> &nbsp;&nbsp;
                        <p class="text-primary">{{ $objednavka->objednavka_id }}</p>
                        <div class="w-100">
                            @if ($objednavka->status == 0)
                                <h2 class="text-danger text-right">
                                    Nevyřízená
                                </h2>
                            @elseif($objednavka->status == 1)
                                <h2 class="text-success text-right">
                                    Vyřízená
                                </h2>
                            @endif
                            <div class="text-white d-block">
                                <p class="text-white text-right my-0 ">Objednáno:
                                    <span class="ml-auto {{ $objednavka->status == 0 ? 'text-danger' : 'text-success' }}">
                                        {{ $objednavka->created_at }} ({{ $objednavka->created_at->diffForHumans() }})
                                    </span>
                                </p>
                            </div>
                            @if ($objednavka->status == 1)
                                <div class="text-white d-block">
                                    <p class="text-white text-right my-0">
                                        Vyřízeno:
                                        <span class="text-success my-0">
                                            ({{ $objednavka->updated_at->diffForHumans() }})
                                        </span>    
                                    </p>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <h3 class="text-white">Osobní údaje</h3>
                                <p class="card-text text-white">
                                    Jméno: 
                                    <span class="text-info">
                                        {{ $objednavka->user_jmeno }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Příjmení:
                                    <span class="text-info">
                                        {{ $objednavka->user_prijmeni }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    E-mail: 
                                    <span class="text-info">
                                        {{ $objednavka->user_email }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Město: 
                                    <span class="text-info">
                                        {{ $objednavka->user_mesto }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Ulice: 
                                    <span class="text-info">
                                        {{ $objednavka->user_ulice }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    PSČ: 
                                    <span class="text-info">
                                        {{ $objednavka->user_psc }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Telefonní číslo:
                                    <span class="text-info">
                                        {{ $objednavka->user_telefon }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Přeprava: 
                                    <span class="text-info">
                                        {{ $objednavka->preprava }}
                                    </span>
                                </p>
                                <p class="card-text text-white">
                                    Platba: 
                                    <span class="text-info">
                                        {{ $objednavka->platba }}
                                    </span>
                                </p>
                            </div>
                            <div class="col-6">
                                <h3 class="text-white">Produkty</h3>
                                @foreach ($objednavka->products as $product)
                                    <div class="row ml-auto">
                                        <p class="text-info">{{ $product->pivot->pocet }}x - &nbsp;</p>
                                        <p class="text-info">
                                            <a href="{{ url('product/' . $product->id) }}">
                                                {{ $product->name }}
                                            </a>
                                        </p>
                                        <p class="text-info">&nbsp; - {{ $product->pivot->size }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @if ($objednavka->status == 0)
                            <div class="row mt-2">
                                <h3 class="text-success mx-3">{{ $objednavka->total }}Kč</h3>
                                <button class="btn btn-danger ml-auto"
                                        wire:click="destroy({{ $objednavka->id }})">
                                            Zamítnout
                                </button>
                                <button class="btn btn-success mx-4"
                                        wire:click="accept({{ $objednavka->id }})">
                                            Příjmout
                                </button>
                            </div>
                        @else
                            <div class="row mt-2">
                                <h3 class="text-success mx-3">{{ $objednavka->total }}Kč</h3>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach

            @if ($paginateRender)
                <div class="my-3 d-flex">
                    <a class="btn btn-primary mx-auto" 
                       wire:click="loadMore">
                       Více objednávek...
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>
