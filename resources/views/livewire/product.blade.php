<div >
    <div class="row justify-content-center">
        <div class="col-lg">
            <div class="product-fotka mx-auto text-center">
                <img height="85%;" width="100%;"
                    @if ($product->fotka == 'none')
                        src="{{ asset('images/product-image-placeholder.jpg') }}"
                    @else
                        src="{{ asset('storage/' . $product->fotka) }}"
                    @endif
                >
            </div>
            <div class="row justify-content-center star-rating h3 mt-auto ">
                <div class="mt-auto" >
                    <i class="fas fa-star mx-1 @if($averageStars >= 1) text-warning @endif"></i>
                    <i class="fas fa-star mx-1 @if($averageStars >= 1.5) text-warning @endif"></i>
                    <i class="fas fa-star mx-1 @if($averageStars >= 2.5) text-warning @endif"></i>
                    <i class="fas fa-star mx-1 @if($averageStars >= 3.5) text-warning @endif"></i>
                    <i class="fas fa-star mx-1 @if($averageStars >= 4.5) text-warning @endif"></i>
                    ({{ $averageStars }})
                </div>
            </div>    
        </div>
        <div class="col-lg-7">
            <div class="product-text mb-3 position-relative">
                <div class="product-jmeno">
                    <h1 class="text-center mx-5 py-3 text-white">{{ $product->name }}</h1>
                    @can('manage-shop')
                        <a href="{{ route('product.stats', [$product->id]) }}">
                            <button class="statbtn">
                                <i class="far fa-chart-bar font-medium"></i>
                            </button>
                        </a>
                        <button class="editbtn" data-toggle="modal" data-target="#adminEditProduct">
                            <i class="fas fa-pencil-alt font-medium"></i>
                        </button>
                    @endcan
                </div>

                <div class="product-popis text-white m-3">
                    @php
                        echo $product->popis;
                    @endphp
                </div>

                <div class="cenabtn">
                    <div class="product-cena mr-3 mt-auto">
                        @if ($product->sleva == 0)
                            <h2 class="text-white mx-4"><span>{{ $product->cena }}</span>Kč</h2>
                        @else
                            <del class="text-white">
                                <h2 class="text-white mx-4 mb-auto "><span>{{ $product->cena }}</span>Kč</h2>
                            </del>
                            <h2 class="mx-4 text-danger mb-auto  "><span>{{ $product->sleva }}</span>Kč</h2>
                        @endif
                    </div>

                    <div class="product-size ml-4 mb-auto ">
                        <p class="text-warning position-absolute " id="productStockMsg" style="bottom:80px;">
                            {{ $message }}
                        </p>
                        <h5 class="text-white ">Vybrat velikost</h5>
                        <select id="productSizeSelect" style="height:40%;" wire:model="size">
                            @foreach ($product->sizes as $size)
                                @if ($size->pivot->stock != 0)
                                    <option value="{{ $size->sizes }}" wire:key="{{ $size->sizes }}">
                                        {{ $size->sizes }}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="product-pocet ml-5">
                        <input class="mb-auto my-4" id="productPocet" type="number" style="width:100%;"
                            wire:model="pocet">
                    </div>

                    <div class="product-btn text-right mb-auto mr-5 ml-auto">
                        <button class="btn btn-outline-success font-small" type="submit" wire:click="store">
                            Koupit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="hr my-3">
    <div class="row my-3">
        <div class="col ratingcol">
            <h1 class="text-center">Recenze ({{ $ratingCount }})</h1> 
                <div class="d-flex ">
                    @if (Auth::check() && Auth::user()->purchasedProduct($product->id) && Auth::user()->hasRating($product))
                        <button class="btn btn-success ml-auto" data-toggle="modal" data-target="#pridatRecenziModal">Přidat recenzi</button>
                    @elseif(!Auth::check())
                        <button class="btn btn-success ml-auto" wire:click="loginFailAlert(0)">Přidat recenzi</button>
                    @elseif(!Auth::user()->purchasedProduct($product->id))
                        <button class="btn btn-success ml-auto" wire:click="loginFailAlert(1)">Přidat recenzi</button>
                    @elseif(!Auth::user()->hasRating($product))
                        <button class="btn btn-success ml-auto" wire:click="loginFailAlert(2)">Přidat recenzi</button>
                    @endif
                </div>
            @foreach($ratings as $rating)
                <div class="rating-box d-flex border {{ ($rating->rating == 0) ? 'border-danger' : 'border-success' }} my-2 position-relative">
                    @if ($rating->rating == 0) 
                        <i class="fas fa-thumbs-down rating-size text-danger position-absolute"></i>
                    @else
                        <i class="fas fa-thumbs-up rating-size  text-success position-absolute"></i>
                    @endif
                        <div class="rating-username" >
                            <h2 class="text-center mt-1">{{ ($rating->display_name) ? $rating->display_name : 'Anonym' }}</h2>
                            <div class="row justify-content-center h5 ">
                                <i class="fas fa-star mx-1 @if($rating->status >= 1) text-warning @endif"></i>
                                <i class="fas fa-star mx-1 @if($rating->status >= 2) text-warning @endif"></i>
                                <i class="fas fa-star mx-1 @if($rating->status >= 3) text-warning @endif"></i>
                                <i class="fas fa-star mx-1 @if($rating->status >= 4) text-warning @endif"></i>
                                <i class="fas fa-star mx-1 @if($rating->status >= 5) text-warning @endif"></i>
                            </div>
                            <p class="text-center">{{ $rating->created_at->format('d M Y') }}</p>    
                        </div>

                        <div class="rating-text d-flex">
                            <h5 class="my-auto ">{{ $rating->text }}</h5>
                        </div>
                    @can('manage-shop')
                        <button type="submit" class="trashbtn my-1 h1" wire:click="destroyRating({{ $rating->id }})">
                            <i class="fas fa-trash m-2"></i>
                        </button>
                    @endcan
                </div>  
            @endforeach
        </div>
    </div>
    @if ($ratingCount > $paginatePocet)
        <div class="row justify-content-center">
            <button class="btn btn-primary mb-3" wire:click="loadMore">Více recenzí</button>
        </div>
    @endif

    @if (Auth::check() && Auth::user()->purchasedProduct($product->id) && Auth::user()->hasRating($product))
        @livewire('pridat-recenzi', ['product' => $product])
    @endif
</div>
