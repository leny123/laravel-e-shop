<div >
    <div class="row justify-content-center my-5">
        <input type="text" style="width:20vw;" wire:model.defer="searchInput">
        <button class="btn btn-primary ml-4" wire:click="search">Vyhledat uživatele</button>
    </div>
    @isset($users)
        <div class="card logincard">
            <div class="card-header text-white text-center">Uživatelé </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr class="text-white">
                            <th scope="col">#</th>
                            <th scope="col">Jméno</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Role</th>
                            <th scope="col">Možnosti</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr class="text-white">
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ implode(', ',$user->roles()->get()->pluck('name')->toArray(),) }}</td>
                                <td class="d-flex justify-content-center">
                                    @can('edit-users')
                                        <a href="{{ route('admin.users.edit', $user->id) }}"><button
                                                class="btn btn-warning float-left mx-1"
                                                type="button">Upravit</button></a>
                                        <form action="{{ route('admin.users.destroy', $user) }}" method="POST" class="float-left ">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            
                                            <button class="btn btn-danger mx-1" type="submit">Smazat</button>
                                        </form>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endisset
</div>
