<div>
    <div class="sort-menu d-flex ">
        <div class="col-auto p-0">
            <div class=" vyhledatprodukt">
                <input class="form-control ml-1 my-1" placeholder="Vyhledat Produkt" wire:model.debounce.500ms="search">
            </div>
        </div>
       
        <div class="col-sm p-0 text-right">
            <select wire:model="znacka" class="ml-auto">
                <option >Vše</option>
                @foreach ($znacky as $znacka)
                    <option >{{ $znacka->display_name }}</option>
                @endforeach
            </select>
                <button class="btn {{ $filter == 'sleva' ? 'btn-warning' : 'btn-outline-warning' }} my-1"
                        wire:click="FilterByPrice('sleva')">
                            Ve slevě
                </button>

                <button class="btn {{ $filter == 'lowest' ? 'btn-success' : 'btn-outline-success' }} my-1"
                        wire:click="FilterByPrice('lowest')">
                            Nejlevnější
                </button>

                <button class="btn {{ $filter == 'highest' ? 'btn-danger' : 'btn-outline-danger' }} my-1 mr-1"
                        wire:click="FilterByPrice('highest')">
                            Nejdražší
                </button>
        </div>
    </div>
    <hr>
    @isset($products) 
        <div class="d-flex justify-content-center">
            <div class="grid d-flex ">
                @foreach ($products as $product)
                    <div class="grid-item m-2">
                        <div class="slevaicon d-flex justify-content-center">
                            @if ($product->sleva != 0)
                                @php
                                    $sleva = $product->cena / 100;
                                    $sleva = $product->sleva / $sleva;
                                    $sleva = 100 - $sleva;
                                @endphp
                                <span class="bg-yellow text-black font-little pt-1 px-1">
                                    @php 
                                        echo round($sleva) . '%'; 
                                    @endphp
                                </span>
                            @endif
                        </div>

                        @can('manage-shop')
                            <button class="trashbtn m-2" type="submit" wire:click="destroy({{ $product->id }})">
                                <i class="fas fa-trash font-medium m-2"></i>
                            </button>
                        @endcan

                        <a href="{{ url('product/' . $product->id) }}">
                            <div class="item-fotka">
                                <img width="100%;" height="100%;"
                                    @if ($product->fotka == "none")
                                        src="{{ asset('images/product-image-placeholder.jpg') }}"
                                    @else
                                        src="{{ asset('storage/' . $product->fotka) }}"
                                    @endif
                                >
                            </div>
                        </a>
                        <div class="shop-karta-bg ">
                            <div class="item-jmeno">
                                <h4 class="py-2">{{ $product->name }}</h4>
                            </div>

                            <div class="item-cena d-flex ">
                                <div class="cena">
                                    @if ($product->sleva == 0)
                                        <h5 class="text-left mx-2 ">{{ $product->cena }}Kč</h5>
                                    @else
                                        <del>
                                            <h5 class="text-left mx-2 ">{{ $product->cena }}Kč</h5>
                                        </del>
                                        <h5 class="text-left m-2 text-danger ">{{ $product->sleva }}Kč </h5>
                                    @endif
                                </div>

                                <div class="gender">
                                    @if ($product->gender == 'f')
                                        <i class="fas fa-female zenymenu ml-2 my-3"></i>
                                    @else
                                        <i class="fas fa-male muzimenu ml-2 my-3"></i>
                                    @endif
                                </div>

                                <div class="shop-btn">
                                    @if (session('kosik.' . $product->id))
                                        <button type="submit" wire:click="removeKosikProduct({{ $product->id }})">
                                            <i class="shop-icon-bought font-small"></i>
                                        </button>
                                    @else
                                        <button type="submit" wire:click="store({{ $product->id }})">
                                            <i class="fas fa-shopping-basket font-medium"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <input name="gender" type="hidden" value="{{ $product->gender }}">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    <div class="row justify-content-center mt-3">
        {{ $products->links() }}
    </div>
    @endisset
</div>
