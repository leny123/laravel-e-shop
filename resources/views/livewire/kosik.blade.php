<div>
    @if ($products != 0)
        <div class="kosik-main d-flex p-2 my-2">
            <div class="kosik-fotka"></div>
            <div class="kosik-name">
                <h3 class="text-white text-center">Název</h3>
            </div>
            <div class="kosik-size">
                <h3 class="text-white text-center">Velikost</h3>
            </div>
            <div class="kosik-pocet">
                <h3 class="text-white text-center">Počet</h3>
            </div>
            <div class="kosik-cena">
                <h3 class="text-white text-center">Cena</h3>
            </div>
            <div class="kosik-btn"></div>
        </div>
        @foreach ($products as $product)
            <div class="kosik-item d-flex p-2 my-2 position-relative">
                <div class="kosik-fotka">
                    <a href="{{ url('product/' . $product->id) }}">
                        <img width="100%;" height="100%;" 
                            src="
                                @if ($product->fotka == "none")
                                    {{ asset('images/product-image-placeholder.jpg') }}
                                @else
                                    {{ asset('storage/' . $product->fotka) }}
                                @endif
                            "
                        >
                    </a>
                </div>

                <div class="kosik-name d-flex justify-content-center">
                    <h5 class="m-auto text-white ">{{ $product->name }}</h5>
                </div>

                <div class="kosik-size  d-flex">
                    <select class="m-auto selectKosik"
                            wire:model="size.{{ $product->id }}" 
                            wire:change="sizeChange({{ $product->id }})">
                                @foreach ($product->sizes as $prodSize)
                                    <option value="{{ $prodSize->sizes }}">
                                        {{ $prodSize->sizes }}
                                    </option>
                                @endforeach
                    </select>
                </div>
              
                <div class="kosik-pocet  d-flex ">
                    <input class="m-auto" type="number" style="width: 35%;"
                           wire:model="pocet.{{ $product->id }}"
                           wire:change="pocetChange({{ $product->id }})">
                </div>

                <div class="kosik-cena d-flex">
                    @if ($product->sleva == 0)
                        <h5 class=" text-white m-auto">
                            {{ $product->cena }}Kč
                        </h5>
                    @else
                        <div class="my-4 mx-auto">
                            <del class="text-white text-center">
                                <h5 class="text-white ">{{ $product->cena }}Kč</h5>
                            </del>
                            <h5 class="text-danger  text-center">{{ $product->sleva }}Kč</h5>
                        </div>
                    @endif
                </div>

                <div class="kosik-btn d-flex">
                    <button class="m-auto" type="submit"
                            wire:click="destroy({{ $product->id }})">
                                <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
        @endforeach
    @else
        <h1 class="text-center ">Košík je prázdný</h1>
    @endif
</div>
