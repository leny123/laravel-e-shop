<div>
    <div class="modal fade" id="pridatRecenziModal" tabindex="-1" role="dialog" aria-labelledby="pridatRecenziModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content text-center modal-bg">
                <div class="modal-header text-center">
                    <h5 class="modal-title mx-auto">Přidat recenzi</h5>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <div class="col mt-auto" >
                            <h4 class="py-2">Vaše jméno pod kterou chcete napsat recenzi. (Můžete nechat prázdné)</h4>
                            <input type="text" 
                                   wire:model.defer="display_name">
                            <h4 class="py-2">Zvolte počet hvězdiček pro tento produkt.</h4>

                            <div class="row justify-content-center h3 rating-stars" dir="rtl">
                                <input type="radio" name="star" class="mx-3" id="rating-star5" value="5" wire:model.defer="stars">
                                    <label for="rating-star5">
                                        <i class="fas fa-star mx-1"></i>
                                    </label>
                                <input type="radio" name="star" class="mx-3" id="rating-star4" value="4" wire:model.defer="stars">
                                    <label for="rating-star4">
                                        <i class="fas fa-star mx-1"></i>
                                    </label>
                                <input type="radio" name="star" class="mx-3" id="rating-star3" value="3" wire:model.defer="stars">
                                    <label for="rating-star3">
                                        <i class="fas fa-star mx-1"></i>
                                    </label>
                                <input type="radio" name="star" class="mx-3" id="rating-star2" value="2" wire:model.defer="stars">
                                    <label for="rating-star2">
                                        <i class="fas fa-star mx-1"></i>
                                    </label>
                                <input type="radio" name="star" class="mx-3" id="rating-star1" value="1" wire:model.defer="stars">
                                    <label for="rating-star1">
                                        <i class="fas fa-star mx-1"></i>
                                    </label>   
                            </div>
                            
                            <h4 class="py-2">Líbí se vám tento produkt?</h4>
                            <div class="row justify-content-center rating-thumb">
                                <input type="radio" name="rating" id="rating-up" value="1" wire:model.defer="rating"> 
                                    <label for="rating-up" id="uplabel">
                                        <i class="fas fa-thumbs-up ratingChoice rating-up m-2"></i>
                                    </label>
                                <input type="radio" name="rating" id="rating-down" value="0" wire:model.defer="rating"> 
                                    <label for="rating-down" id="downlabel">
                                        <i class="fas fa-thumbs-down ratingChoice rating-down m-2"></i>
                                    </label>
                            </div>
                            <h4 class="py-2">Recenze (Můžete nechat prázdné)</h4>
                            <input type="text" wire:model.defer="text">    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary my-2" data-dismiss="modal" wire:click="pridatRecenzi">Přidat Recenzi</button>
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Zavřít</button>
                </div>  
            </div>
        </div>
    </div>
</div>
