@extends('layouts.app')

@can('manage-shop')
    @include('layouts.modals.pridatProduktModal')
    @include('layouts.modals.pridatKategorieModal')
    @include('layouts.modals.pridatSizeModal')
    @include('layouts.modals.pridatZnackaModal')
    @include('layouts.admin')
@endcan

@include('layouts.productMenu')
@include('layouts.sweetalert.TitleAlert')

@section('content')
        <div class="container mt-5">
            <h1 class=" text-center font-big text-black">{{ config('app.name') }}</h1>
            <hr class="hr my-3">
            <div class="row">
                <div class="col-3 menucol d-md">
                    <div class="sticky-col">
                        @yield('productmenu')
                    </div>
                </div>

                <div class="col productscol">
                    @livewire('products', ['urls' => $urls])
                </div>
            </div>
        </div>
    @can('manage-shop')
        @yield('AdminPridatListModal')
        @yield('AdminPridatProduktModal')
        @yield('AdminPridatSizeModal')
        @yield('AdminPridatZnackaModal')
    @endcan
@endsection
